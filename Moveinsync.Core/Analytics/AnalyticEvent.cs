﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Moveinsync.Core.Analytics
{
    public class AnalyticEvent
    {
        public string EventName { get; set; }
        public List<KeyValuePair<string, string>> Parameters { get; set; }

        public AnalyticEvent()
        {
            Parameters = new List<KeyValuePair<string, string>>();
        }
    }
}
