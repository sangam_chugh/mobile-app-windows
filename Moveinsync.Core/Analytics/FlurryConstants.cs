﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Analytics
{
    public  class FlurryConstants
    {
        public static class Event
        { 
            public const string Launch = "Launch";
            public const string BackgroundForeground = "BackgroundForeground";
            public const string FirstLaunches = "FirstLaunches";
            public const string Upgrade = "Upgrade";
            public const string UpgradeVersion = "Upgrade";
            public const string NetworkType = "NetworkType";
            public const string EndPointNotification = "EndPointNotification";
            public const string EmployeeMobile = "EmployeeMobile";//done           
            public const string StartSession = "StartSession";//done

            //---------------------------------------------------------------------------------
            public const string ProfilePanorma_LoadTime = "ProfilePanorma_LoadTime";//done
            public const string SchedulePanorma_LoadTime = "SchedulePanorma_LoadTime";//done
            public const string TrackCabPanorma_LoadTime = "TrackCabPanorma_LoadTime";//done

            //---------------------------------------------------------------------------------
            public const string ProfilePanorma_PageVisit = "ProfilePanorma_PageVisit";//done
            public const string SchedulePanorma_PageVisit = "SchedulePanorma_PageVisit";//done
            public const string TrackCabPanorma_PageVisit = "TrackCabPanorma_PageVisit";//done
            public const string Registration_PageVisit = "RegistrationPanorama_PageVisit";//done
            public const string Registration_LoadTime = "RegistrationPanorama_LoadTime";
          
            public const string ResendOTP_Click = "ResendOTP_Click";//done
            public const string ChangeMobile_Click = "ChangeMobile_Click";//done
            public const string ConfirmOTP_Click = "ConfirmOTP_Click";//done
            public const string OTPSuccessful = "OTPSuccessful";//done
            public const string OTPFailed = "OTPFailed";//done
            public const string GETOTP_Click = "GETOTP_Click";//done

            //---------------------------------------------------------------------------------

            public const string TrackCabPanorma_TripInProgress = "TrackCabPanorma_TripInProgress";//done
            public const string EditTrip_Click = "EditTrip_Click";// done
            public const string CancelTrip_Click = "CancelTrip_Click";// done
            public const string EditSchedulePage_LoadTime = "EditSchedulePage_LoadTime";
            public const string EditSchedule_PageVisit = "EditSchedule_PageVisit";// done
            public const string SchedulePageVisit = "SchedulePageVisit";//done
            public const string SOSHold = "SOSHold";//done
            public const string SOSRaised_Successful = "SOSRaised_Successful";//done
            //-----------------------------------------------------------------------
            public const string NoScheduleFound = "NoScheduleFound";//done

            public const string Alerts = "Alert";//done
            public const string ExitEvent = "Exit";//done

            public const string TermsAndconditions_Click = "TermsAndconditions_Click";//done
            public const string TermsAndconditions_Visit = "TermsAndconditions_Visit";//done

            public const string PrivacyPolicy_Click = "PrivacyPolicy_Click_Click";//done
            public const string PrivacyPolicy_Visit = "PrivacyPolicy_Visit";//done

            public const string AboutUs_Click = "AboutUs_Click";//done
            public const string AboutUs_Visit = "AboutUs_Visit";//done

            public const string ProfileLoaded = "ProfileLoaded";//done
        }

        public class EventParameter // format is Event_Parameter for specific ones
        {
            public const string SOSClicked_LocationNotEnabled = "LocationNotEnabled";
           
            public const string EmployeeMobile = "EmployeeMobile";
            public const string UUID = "UUID";
            public const string OTP = "OTP";
            public const string UnableToGetLocation = "UnableToGetLocation";//LocationOfSOSLatitude
            public const string LocationOfSOSLatitude = "LocationOfSOSLatitude";
            public const string LocationOfSOSLongitude = "LocationOfSOSLongitude";
            public const string TripID = "TripID";
            public const string Driver = "Driver";
            public const string VehicleId = "VehicleId";
            public const string UnixTimestampOfSOS = "UnixTimestampOfSOS";
            public const string SMSComposed = "SMSComposed";
            public const string LoadTime_TimeSpanInMilliSeconds = "LoadTime_TimeSpanInMilliSeconds";
        }
    }
}
