﻿using Moveinsync.Core.Analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.EventArguments
{
    public class AnalyticEventArgs : EventArgs
    {
        public AnalyticEvent EventLog { get; private set; }

        public AnalyticEventArgs(AnalyticEvent eventLog)
        {
            EventLog = eventLog;
        }
    }
}
