﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.EventArguments
{
   public  class MapArguments:EventArgs
    {
       public double LatestLatitude { get; set; }
       public double LatestLongitude { get; set; }
       public double LatestBearing { get; set; }
       public DateTime LatestUpdatedTime { get; set; }
       public double LatestSpeed { get; set; }

       public string LastSeentime { get; set; }
    }
}
