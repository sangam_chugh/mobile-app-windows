﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.EventArguments
{
    public class PanicArgs : EventArgs
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
    public class PanicSuccessArgs : EventArgs
    {
        public string Panic_Success_Message { get; set; }
    }

}
