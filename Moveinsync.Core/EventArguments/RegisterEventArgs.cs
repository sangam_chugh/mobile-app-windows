﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.EventArguments
{
    public class RegisterEventArgs : EventArgs
    {
        public bool IsRegisterd { get; set; }
        public string ErrorMessage { get; set; }
    }
}
