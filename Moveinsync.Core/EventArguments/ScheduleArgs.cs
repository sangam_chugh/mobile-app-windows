﻿using Moveinsync.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.EventArguments
{
    public class ScheduleArgs:EventArgs
    {

        public string  Message { get; set; }
    }
}
