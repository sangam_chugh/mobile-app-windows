﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moveinsync.Core.Model;

namespace Moveinsync.Core.EventArguments
{
    public class TrackCabEventArgs:EventArgs
    {
        public bool IsTrackDetailsAvailable { get; set; }
        public string Error { get; set; }
        public TrackMilestone objTrack { get; set; }
    }
}
