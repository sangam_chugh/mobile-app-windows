﻿using Moveinsync.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.IService
{
    public interface IEmployeeProfileService
    {
        Task<EmployeeProfile> GetProfile();
    }
}
