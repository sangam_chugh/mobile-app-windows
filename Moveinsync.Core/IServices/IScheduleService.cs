﻿using Moveinsync.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.IServices
{
    public interface IScheduleService
    {
        Task<ICollection<Schedule>> GetSchedule();
        Task<string> UpdateScheduleItem(Schedule schedule, Shift item);
        Task<string> CancelScheduleItem(Schedule schedule, string isLoginOrLogout);
    }
}
