﻿using Moveinsync.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.IServices
{
    public interface ISettingsService
    {
        Task<Settings> GetSettings();
    }
}
