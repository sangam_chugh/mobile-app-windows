﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moveinsync.Core.Model;

namespace Moveinsync.Core.IServices
{

    public interface IPanicService
    {
        Task<string> RaisePanic(Panic item);
    }
}
