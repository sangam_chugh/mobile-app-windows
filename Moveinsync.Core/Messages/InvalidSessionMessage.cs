﻿using Cirrious.MvvmCross.Plugins.Messenger;
using Moveinsync.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Messages
{
    public class InvalidSessionMessage : MvxMessage
    {
        public CustomError Error { get; private set; }
        public InvalidSessionMessage(object sender, CustomError error)
            : base(sender)
        {
            Error = error;
        }
    }
}
