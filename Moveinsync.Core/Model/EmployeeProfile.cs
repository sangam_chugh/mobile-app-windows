﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Model
{
    public class EmployeeProfile
    {
        public string EmployeeId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public List<object> EmergencyContactNumbers { get; set; }
        public string Organization { get; set; }
    }
}
