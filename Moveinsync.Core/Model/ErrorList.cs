﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Model
{
    public class CustomError : Exception
    {
        public ErrorModel Error { get; set; }
        public bool IsConnectivityError { get; set; }
        public bool IsAuthorizationError { get; set; }

        public override string StackTrace
        {
            get
            {
                string connectivityError = IsConnectivityError ? "Connectivity Error - Cannot connect to internet or server is down" + Environment.NewLine : string.Empty;
                string authorizationError = IsAuthorizationError ? "Authorization Error - The device is not authorized for access" + Environment.NewLine : string.Empty;
                if (Error != null)
                {
                    string errorCode = Error.ApiError != null && Error.ApiError.Code >= 0 ? string.Format("Error Code - {0}{1}", Error.ApiError.Code, Environment.NewLine) : string.Empty;
                    string errorPhrase = Error.ApiError != null && !string.IsNullOrWhiteSpace(Error.ApiError.Message) ? string.Format("Error Message - {0}{1}", Error.ApiError.Message, Environment.NewLine) : string.Empty;
                    string httpStatusMessage = string.IsNullOrWhiteSpace(Error.HttpStatusMessage) ? string.Empty : string.Format("HTTP Status Message - {0}{1}", Error.HttpStatusMessage, Environment.NewLine);
                    string apiReasonPhrase = string.IsNullOrWhiteSpace(Error.ReasonPhrase) ? string.Empty : string.Format("API Reason Phrase - {0}{1}", Error.ReasonPhrase, Environment.NewLine);

                    string apiErrorTrace = string.Format("Error when API was called.{0}{1}{2}{3}{4}", Environment.NewLine, errorCode, errorPhrase, httpStatusMessage, apiReasonPhrase);

                    return string.Format("{0}{1}{2}{3}{1}Original Stack Trace{1}{4}", apiErrorTrace, Environment.NewLine, connectivityError, authorizationError, base.StackTrace);
                }
                return string.Format("{0}{1}{3}Original Stack Trace{3}{2}", connectivityError, authorizationError, base.StackTrace, Environment.NewLine);
            }
        }
    }
    public class ErrorModel
    {
        public ApiErrorModel ApiError { get; set; }
        public string HttpStatusMessage { get; set; }
        public string ReasonPhrase { get; set; }

    }
    public class ApiErrorModel
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
