﻿using Moveinsync.Core.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Model
{
    public class Schedule
    {
        public string Date { get; set; }
        public ScheduleItem Login { get; set; }
        public ScheduleItem Logout { get; set; }
        public bool Exception { get; set; }
       
        public DateTime DateValue
        {
            get
            {
                return Convert.ToDateTime(Date).Date;
            }
        }

        public string DisplayDate
        {
            get
            {
                var dateValue = DateValue;
                if (dateValue == DateTime.Today.Date)
                {
                    return "Today"; //Todo :Load from resx
                }
                else if (dateValue == DateTime.Today.Date.AddDays(1))
                {
                    return "Tomorrow"; //Todo :Load from resx
                }
                return Date;
            }
        }
        public ObservableCollection<ScheduleItem> Shifts
        {
            get
            {
                var shiftList = new List<ScheduleItem>();
                Login.ParentSchedule = this;
                Logout.ParentSchedule = this;
                shiftList.Add(Login);
                shiftList.Add(Logout);
                if (Login.Hours < 0 || Logout.Hours < 0)
                {
                    return new ObservableCollection<ScheduleItem>(shiftList);
                }
                return new ObservableCollection<ScheduleItem>(shiftList.OrderBy(c => (c.Hours * 60 + c.Minutes)));
            }
        }
    }
    public class Office
    {
        public string Guid { get; set; }                        //"LOprepro-dTes-t$00-0000-000000000001",
        public string GeoCord { get; set; }
        public string Address { get; set; }                   //"geoCord": "12.912274,77.643679",
        public string Landmark { get; set; }
    }

    public class ScheduleItem
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public string Venue { get; set; }
        public string State { get; set; }
        public string Type { get; set; }
        public string ForegroundColor { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public Schedule ParentSchedule { get; set; }

        public string PickupAddress { get; set; }
        public string DropAddress { get; set; }

        public string Display
        {
            get
            {
                if (Hours >= 0)
                {
                    if (string.Equals(State, "Scheduled", StringComparison.CurrentCultureIgnoreCase))
                    {

                        return string.Format("{0}:{1}", Hours.ToString().PadLeft(2, '0'), Minutes.ToString().PadLeft(2, '0'));

                    }
                    else
                    {
                        return State;
                    }

                }
                else if (Hours == -1)
                {
                    if (string.IsNullOrEmpty(State))
                    {
                        return "Not Scheduled"; //Todo :Move this to regx.
                    }
                    else
                    {
                        return State;
                    }
                }
                else if (Hours == -2)
                {
                    return "Leave"; //Todo :Move this to regx.
                }
                return string.Empty;
            }
        }
    }
    public class ScheduleItemPost
    {
        public int hours { get; set; }
        public int minutes { get; set; }
        public string state { get; set; }
        public string type { get; set; }
        //public string venue { get; set; }
    }
    public class SchedulePostLogin
    {
        public string date { get; set; }
        public ScheduleItemPost login { get; set; }
        public bool exception { get; set; }
    }
    public class SchedulePostLogout
    {
        public string date { get; set; }
        public ScheduleItemPost logout { get; set; }
        public bool exception { get; set; }
    }

}
