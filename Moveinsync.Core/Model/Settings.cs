﻿using System;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.File;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Cirrious.CrossCore;

namespace Moveinsync.Core.Model
{
    public class Settings
    {
        public bool IsProfileEnabled { get; set; }
        public bool IsTripDetailsEnabled { get; set; }
        public bool IsVehicleTrackingEnabled { get; set; }
        public bool IsScheduleEnabled { get; set; }
        public bool IsSOSEnabled { get; set; }
       
        public VehicleTracking VehicleTracking { get; set; }
        public ScheduleSetting Schedule { get; set; }
        public int? LongPressDuration { get; set; }
     
    }
   
  
    public class VehicleTracking
    {       
        public int Frequency { get; set; }
        public int MapMilestoneDrawTime { get; set; }
        public int RetryTimeLowerLimit { get; set; }
        public int RetryTimeUpperLimit { get; set; }
    }

    public class ScheduleSetting
    {        
        public int DaysBefore { get; set; }
        public int DaysAfter { get; set; }
        public bool Editable { get; set; }
        public bool Cancellable { get; set; }
        public bool MultiOfficeEnabled { get; set; }
    }
   

}
