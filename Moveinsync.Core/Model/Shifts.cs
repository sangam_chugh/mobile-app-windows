﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Model
{
    public class Shifts
    {
        public List<Shift> LogoutShifts { get; set; }
        public List<Shift> LoginShifts { get; set; }
    }
    public class Shift
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public string State { get; set; }
        public string Type { get; set; }

        public string Display
        {
            get
            {
                if (Hours >= 0)
                {
                    return string.Format("{0}:{1}", Hours.ToString().PadLeft(2, '0'), Minutes.ToString().PadLeft(2, '0'));
                }
                else if(Hours == -2)
                {
                    return "Leave"; //Todo :Move this to regx.
                }
                return string.Empty;
            }
        }
    }

  

}
