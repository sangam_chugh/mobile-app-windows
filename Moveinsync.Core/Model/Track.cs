﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Model
{
    public class TrackMilestone
    {
        public long Time { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Bearing { get; set; }
        public double Speed { get; set; }

        public string DisplaySpeed //Distance is always provided by server in meter/sec
        {
            get
            {
                return Math.Round(Speed * 3.6, 1).ToString() + " Km/hr";
            }
        }
    }
}
