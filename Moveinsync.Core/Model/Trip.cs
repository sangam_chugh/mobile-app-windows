﻿using Moveinsync.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Model
{
    public class Trip
    {
        public Pickup Pickup { get; set; }
        public Drop Drop { get; set; }
        public Cab Cab { get; set; }

  
        public Trip()
        {
            Pickup = new Pickup();
            Drop = new Drop();
            Cab = new Cab();
        }

    }
    
    
    public class Pickup
    {
        public long Time { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Address { get; set; }
        public string Landmark { get; set; }

        public string DateTime
        {
            get
            {
                return UnixTimeStamp.ConvertFromUnixTimestamp(Time / 1000).ToString("MMM dd / HH:mm").Replace("-","/");
            }
        }
    }

    public class Drop
    {
        public long Time { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Address { get; set; }
        public string Landmark { get; set; }

        public string DateTime
        {
            get
            {
                return UnixTimeStamp.ConvertFromUnixTimestamp(Time / 1000).ToString("MMM dd / HH:mm").Replace("-", "/");
            }
        }
    }

    public class Cab
    {
        public string misVehicleId { get; set; }
        public string vehicleRegNumber { get; set; }
        public string driverName { get; set; }
        public string driverContactNumber { get; set; }

    }

    
}
