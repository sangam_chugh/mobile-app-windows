﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.ServiceModel
{
    public class BaseServiceModel
    {
        public string code { get; set; }
        public string message { get; set; }
    }
}
