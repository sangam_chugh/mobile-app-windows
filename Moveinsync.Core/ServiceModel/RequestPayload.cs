﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.ServiceModel
{
    public class RequestPayload
    {
        public class Register
        {
            public string reg_phone_number { get; set; }
        }
        public class Validation
        {
            public string reg_token { get; set; }
        }
    }
}
