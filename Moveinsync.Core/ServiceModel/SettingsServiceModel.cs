﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.ServiceModel
{
    public class SettingsServiceModel
    {
        public List<SettingObject> Features { get; set; } 
    }

    public class SettingObject
    {
        public string id { get; set; }
        public bool enabled { get; set; }

        public int? frequency { get; set; }
        public int? MapMilestoneDrawTime { get; set; }
        public int? RetryTimeLowerLimit { get; set; }
        public int? RetryTimeUpperLimit { get; set; }

        public int? daysBefore { get; set; }
        public int? daysAfter { get; set; }
        public bool? editable { get; set; }
        public bool? cancellable { get; set; }
        public bool? multiOfficeEnabled { get; set; }

        public int? longPressDuration { get; set; }
    }
}
