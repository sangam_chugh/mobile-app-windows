﻿using Moveinsync.Core.IService;
using Moveinsync.Core.Model;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Services
{
    public class EmployeeProfileService : IEmployeeProfileService
    {
        public async Task<EmployeeProfile> GetProfile()
        {
            EmployeeProfile objEmployeeProfile=new EmployeeProfile();
            string getProfileURI = new Uri(Constants.Service.BaseUri + Constants.Service.Employee.GetProfile).AbsoluteUri;
            HttpRequest<EmployeeProfile> client = new HttpRequest<EmployeeProfile>(getProfileURI, RequestMethod.GET, null, null, null, false,null, UserSession.Profile.UserAuthorization);
            return await client.GetResponseForJSON();
        }
    }
}
