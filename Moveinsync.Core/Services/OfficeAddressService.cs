﻿using Moveinsync.Core.IServices;
using Moveinsync.Core.Model;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Services
{
    public class OfficeAddressService : IOfficeAddressService
    {

        public async Task<List<Office>> GetOffices()
        {
            if (UserSession.Settings != null && UserSession.Settings.Schedule != null && UserSession.Settings.Schedule.MultiOfficeEnabled)
            {
                string getScheduleURI = new Uri(Constants.Service.BaseUri + Constants.Service.Schedules.GetOffices).AbsoluteUri;

                HttpRequest<List<Office>> client = new HttpRequest<List<Office>>(getScheduleURI, RequestMethod.GET, null, null, null, false, null, UserSession.Profile.UserAuthorization);
                return await client.GetResponseForJSON();
            }
            return null;
        }


    }
}
