﻿using Moveinsync.Core.IServices;
using Moveinsync.Core.Model;
using Moveinsync.Core.ServiceModel;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Services
{
    public class PanicService : IPanicService
    {
        public async Task<string> RaisePanic(Panic item)
        {
            string getPanicURI = new Uri(Constants.Service.BaseUri + Constants.Service.Panic.GetPanic).AbsoluteUri;
            string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(item);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var getSOSRequest = new HttpRequest<string>(getPanicURI, RequestMethod.POST, null, null, null, false, content, UserSession.Profile.UserAuthorization);
            return await getSOSRequest.GetResponseString();
        }




    }
}
