﻿using Moveinsync.Core.IServices;
using Moveinsync.Core.Model;
using Moveinsync.Core.ServiceModel;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Services
{
    public class RegistrationService : IRegistrationService
    {
        public async Task<Registration> GetRegistered(string MobileNo)
        {
            string getRegistrationURI = new Uri(Constants.Service.BaseUri + Constants.Service.Registrations.GetRegistered).AbsoluteUri;
            RequestPayload.Register reqPayload = new RequestPayload.Register();
            reqPayload.reg_phone_number = MobileNo;  // Later take it from parameter
            var jsonString = JsonConvert.SerializeObject(reqPayload);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var getRegistrationRequest = new HttpRequest<BaseServiceModel>(getRegistrationURI, RequestMethod.POST, null, null, null, false, content, UserSession.CreateUUID());
            BaseServiceModel response = await getRegistrationRequest.GetResponseForJSON();
            if (response == null)
            {
                return new Registration() { IsRegistered = true };
            }
            else
            {
                return new Registration() { IsRegistered = false, ErrorCode = response.code, ErrorMessage = response.message };
            }

        }

    }
}
