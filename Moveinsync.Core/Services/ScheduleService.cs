﻿using Moveinsync.Core.IServices;
using Moveinsync.Core.Model;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Services
{
    public class ScheduleService : IScheduleService
    {

        public async Task<ICollection<Schedule>> GetSchedule()
        {
            if (UserSession.Settings.IsScheduleEnabled)
            {
                var currentDate = DateTime.Now;
                var startDate = currentDate; // currentDate.AddDays(UserSession.Settings.Schedule.DaysBefore * -1);
                var endDate = currentDate.AddDays(UserSession.Settings.Schedule.DaysAfter);

                string getScheduleURI = new Uri(Constants.Service.BaseUri + Constants.Service.Schedules.GetSchedules).AbsoluteUri;
               
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("startDate", string.Format("\"{0}\"", startDate.ToString("dd/MM/yyyy")).Replace("-","/"));
                param.Add("endDate", string.Format("\"{0}\"", endDate.ToString("dd/MM/yyyy")).Replace("-", "/"));
                
                HttpRequest<ICollection<Schedule>> client = new HttpRequest<ICollection<Schedule>>(getScheduleURI, RequestMethod.GET, null, param, null, false, null, UserSession.Profile.UserAuthorization);
                var response = await client.GetResponseForJSON();


                return (ICollection<Schedule>)response;      
            }
            else
            {
                return new List<Schedule>();
            }
            
            
        }


        public async Task<string> UpdateScheduleItem(Schedule schedule, Shift item)
        {   
            string response=string.Empty;
            if (UserSession.Settings.IsScheduleEnabled && UserSession.Settings.Schedule.Editable)
                {
                 string getScheduleURI = new Uri(Constants.Service.BaseUri + Constants.Service.Schedules.GetSchedules).AbsoluteUri;

                   
                    var content = new StringContent(GetPostScheduleContent(schedule,string.Empty,item), Encoding.UTF8, "application/json");
                    var urlParams = new Dictionary<string, string>();
                    urlParams.Add("action", "edit");

                    HttpRequest<string> client = new HttpRequest<string>(getScheduleURI, RequestMethod.POST, null, urlParams, null, false, content, UserSession.Profile.UserAuthorization);
                    response = await client.GetResponseForJSON();
                    return response;
                }
            return response;
            }




        public async Task<string> CancelScheduleItem(Schedule schedule, string isLoginOrLogout)
        {
            string response = string.Empty;
            if (UserSession.Settings.IsScheduleEnabled && UserSession.Settings.Schedule.Cancellable)
            {
                string getScheduleURI = new Uri(Constants.Service.BaseUri + Constants.Service.Schedules.GetSchedules).AbsoluteUri;
                

                //var jsonString = JsonConvert.SerializeObject(reqPayload);
                var content = new StringContent(GetPostScheduleContent(schedule, isLoginOrLogout,null), Encoding.UTF8, "application/json");
                var urlParams = new Dictionary<string, string>();
                urlParams.Add("action", "cancel");

                HttpRequest<string> client = new HttpRequest<string>(getScheduleURI, RequestMethod.POST, null, urlParams, null, false, content, UserSession.Profile.UserAuthorization);
                response = await client.GetResponseForJSON();
                return response;
            }

            return response;
        }


        public string GetPostScheduleContent(Schedule schedule, string isLoginOrLogout,Shift item=null)
        {
            string jsonString = string.Empty;
            if (item == null)//means biuld content for cancel
            {
                if (isLoginOrLogout == "LOGIN")
                {
                    SchedulePostLogin objSchedulePostLogin = new SchedulePostLogin();
                    objSchedulePostLogin.date = schedule.Date;
                    objSchedulePostLogin.login = new ScheduleItemPost();
                    objSchedulePostLogin.login.hours = schedule.Login.Hours;
                    objSchedulePostLogin.login.minutes = schedule.Login.Minutes;
                    objSchedulePostLogin.login.state = "CANCELLED";
                    objSchedulePostLogin.login.type = schedule.Login.Type;
                    objSchedulePostLogin.exception = false;
                    List<SchedulePostLogin> objSchedulePostLoginList = new List<SchedulePostLogin>();
                    objSchedulePostLoginList.Add(objSchedulePostLogin);
                    jsonString = JsonConvert.SerializeObject(objSchedulePostLoginList);
                }
                else
                {
                    SchedulePostLogout objSchedulePostLogout = new SchedulePostLogout();
                    objSchedulePostLogout.date = schedule.Date;
                    objSchedulePostLogout.logout = new ScheduleItemPost();
                    objSchedulePostLogout.logout.hours = schedule.Logout.Hours;
                    objSchedulePostLogout.logout.minutes = schedule.Logout.Minutes;
                    objSchedulePostLogout.logout.state = "CANCELLED";
                    objSchedulePostLogout.logout.type = schedule.Logout.Type;
                    objSchedulePostLogout.exception = false;
                    List<SchedulePostLogout> objSchedulePostLogoutList = new List<SchedulePostLogout>();
                    objSchedulePostLogoutList.Add(objSchedulePostLogout);
                    jsonString = JsonConvert.SerializeObject(objSchedulePostLogoutList);
                }
            }
            else //build content for edit schedule 
            {
                if (item.Type == "LOGIN")
                {
                    SchedulePostLogin objSchedulePostLogin = new SchedulePostLogin();
                    objSchedulePostLogin.date = schedule.Date;
                    objSchedulePostLogin.login = new ScheduleItemPost();
                    objSchedulePostLogin.login.hours = item.Hours;
                    objSchedulePostLogin.login.minutes = item.Minutes;
                    objSchedulePostLogin.login.state = "SCHEDULED";
                    objSchedulePostLogin.login.type = item.Type;
                    objSchedulePostLogin.exception = false;
                    List<SchedulePostLogin> objSchedulePostLoginList = new List<SchedulePostLogin>();
                    objSchedulePostLoginList.Add(objSchedulePostLogin);
                    jsonString = JsonConvert.SerializeObject(objSchedulePostLoginList);
                }
                else
                {
                    SchedulePostLogout objSchedulePostLogout = new SchedulePostLogout();
                    objSchedulePostLogout.date = schedule.Date;
                    objSchedulePostLogout.logout = new ScheduleItemPost();
                    objSchedulePostLogout.logout.hours = item.Hours;
                    objSchedulePostLogout.logout.minutes = item.Minutes;
                    objSchedulePostLogout.logout.state = "SCHEDULED";
                    objSchedulePostLogout.logout.type = item.Type;
                    objSchedulePostLogout.exception = false;
                    List<SchedulePostLogout> objSchedulePostLogoutList = new List<SchedulePostLogout>();
                    objSchedulePostLogoutList.Add(objSchedulePostLogout);
                    jsonString = JsonConvert.SerializeObject(objSchedulePostLogoutList);
                }

            }

            return jsonString;
        }
    }
}
