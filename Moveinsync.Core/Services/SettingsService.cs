﻿using Moveinsync.Core.IServices;
using Moveinsync.Core.Model;
using Moveinsync.Core.ServiceModel;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Services
{
    public class SettingsService : ISettingsService
    {
        public async Task<Settings> GetSettings()
        {
            string getSettingsURI = new Uri(Constants.Service.BaseUri + Constants.Service.Settings.GetSetings).AbsoluteUri;
            var getSettingsRequest = new HttpRequest<SettingsServiceModel>(getSettingsURI, RequestMethod.GET, null, null, null, false, null, UserSession.Profile.UserAuthorization);
            var serviceModel = await getSettingsRequest.GetResponseForJSON();

            Settings settings = new Settings();
            if (serviceModel.Features != null)
            {
                foreach (SettingObject item in serviceModel.Features)
                {
                    switch (item.id) //Todo : Convert below values to constants
                    {
                        case Moveinsync.Core.Utility.Constants.SettingTypes.Profile:
                            settings.IsProfileEnabled = item.enabled;
                            break;
                        case Moveinsync.Core.Utility.Constants.SettingTypes.Schedule:
                            settings.IsScheduleEnabled = item.enabled;
                            if (item.enabled)
                            {
                                settings.Schedule = new ScheduleSetting() //Todo : Cann do null check and put defualt value if required
                                {
                                    Cancellable = item.cancellable.Value,
                                    DaysAfter = item.daysAfter.Value,
                                    DaysBefore = item.daysBefore.Value,
                                    Editable = item.editable.Value,
                                    MultiOfficeEnabled = item.multiOfficeEnabled.HasValue ? item.multiOfficeEnabled.Value : false
                                };
                            }                   
                            break;
                        case Moveinsync.Core.Utility.Constants.SettingTypes.SOS:
                            settings.IsSOSEnabled = item.enabled;
                            settings.LongPressDuration = item.longPressDuration;
                            break;
                        case Moveinsync.Core.Utility.Constants.SettingTypes.TripDetails:
                            settings.IsTripDetailsEnabled = item.enabled;
                            break;
                        case Moveinsync.Core.Utility.Constants.SettingTypes.VehicleTracking:
                            settings.IsVehicleTrackingEnabled = item.enabled;
                            if (item.enabled)
                            {
                                settings.VehicleTracking = new VehicleTracking() //Todo : Can do null check and put defualt value if required
                                {
                                    Frequency = item.frequency.Value,
                                    MapMilestoneDrawTime = item.MapMilestoneDrawTime.Value,
                                    RetryTimeLowerLimit = item.RetryTimeLowerLimit.Value,
                                    RetryTimeUpperLimit = item.RetryTimeUpperLimit.Value
                                };
                            }                            
                            break;

                    }
                }
            }

            return settings;

        }
    }
}
