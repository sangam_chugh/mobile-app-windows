﻿using Moveinsync.Core.IServices;
using Moveinsync.Core.Model;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Services
{
    public class ShiftService : IShiftService
    {
        public async Task<Shifts> GetShifts(DateTime currentDate)
        {
            string getShiftsURI = new Uri(Constants.Service.BaseUri + Constants.Service.Shift.GetShift).AbsoluteUri;
            Dictionary<string, string> param = new Dictionary<string, string>();           
            param.Add("startDate", string.Format("\"{0}\"", currentDate.ToString("dd/MM/yyyy")).Replace("-","/"));
            param.Add("endDate", string.Format("\"{0}\"", currentDate.ToString("dd/MM/yyyy")).Replace("-","/"));
            var getScheduleRequest = new HttpRequest<Shifts>(getShiftsURI, RequestMethod.GET, null, param, null, false, null, UserSession.Profile.UserAuthorization);
            return await getScheduleRequest.GetResponseForJSON();
        }
    }
}
