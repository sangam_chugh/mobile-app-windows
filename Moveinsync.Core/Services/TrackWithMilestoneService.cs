﻿using Moveinsync.Core.IServices;
using Moveinsync.Core.Model;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Services
{
    public class TrackWithMilestoneService : ITrackWithMilestoneService
    {
        public async Task<TrackMilestone> GetTrackWithMilestones(string vehicleRegNumber, string misVehicleId)
        {
            string getMilestoneTripURI = new Uri(Constants.Service.BaseUri + Constants.Service.TripWithMilestone.GetMilestoneTrip).AbsoluteUri;
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("vehicleRegNumber", vehicleRegNumber); //use parameters later 
            param.Add("misVehicleId", misVehicleId);
            var getTripWithMilestonesRequest = new HttpRequest<List<TrackMilestone>>(getMilestoneTripURI, RequestMethod.GET, null, param, null, false, null, UserSession.Profile.UserAuthorization);
            
            var trackList = await getTripWithMilestonesRequest.GetResponseForJSON();
            if (trackList != null && trackList.Count > 0)
            {
                return trackList.First();
            }
            return null;
        }
    }
}
