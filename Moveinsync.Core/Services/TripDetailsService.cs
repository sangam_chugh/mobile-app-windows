﻿using Moveinsync.Core.IServices;
using Moveinsync.Core.Model;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Services
{
    public class TripDetailsService : ITripDetailsService
    {

        public async Task<Trip> GetTripDetais()
        {

            Trip objTrip = new Trip();
            string getTripDetailURI = new Uri(Constants.Service.BaseUri + Constants.Service.TripDetail.GetTripDetail).AbsoluteUri;

            HttpRequest<Trip> client = new HttpRequest<Trip>(getTripDetailURI, RequestMethod.GET, null, null, null, false, null, UserSession.Profile.UserAuthorization);
            return await client.GetResponseForJSON();



        }
    }
}
