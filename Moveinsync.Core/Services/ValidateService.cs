﻿using Moveinsync.Core.IServices;
using Moveinsync.Core.Model;
using Moveinsync.Core.ServiceModel;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Services
{
    public class ValidateService : IValidateService
    {
        public async Task<bool> GetValidated(string Token)
        {
       
            string getValidationURI = new Uri(Constants.Service.BaseUri + Constants.Service.Validation.GetValidate).AbsoluteUri;
            RequestPayload.Validation reqPayload = new RequestPayload.Validation();
            reqPayload.reg_token = Token;  // Later take it from parameter
            var jsonString = JsonConvert.SerializeObject(reqPayload);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            HttpRequest<Validate> client = new HttpRequest<Validate>(getValidationURI, RequestMethod.POST, null, null, null, false, content, UserSession.TempUUID);
            var response = await client.GetResponseString();
           if(response != null)
           {
               return true;
           }
           else
           {
               return false;
           }

        }
    }
}
