﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.User
{
    public class UserProfile
    {
        public UserProfile()
        {

        }
        public UserProfile(string userAuthorization, string userAuthorizationConfirmationKey)
        {
            UserAuthorization = userAuthorization;
            UserAuthorizationConfirmationKey = userAuthorizationConfirmationKey;
        }
        public string UserAuthorization { get; set; }
        public string UserAuthorizationConfirmationKey { get; private set; }

    }
}
