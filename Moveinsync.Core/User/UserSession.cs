﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.File;
using Moveinsync.Core.Model;
using Moveinsync.Core.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace Moveinsync.Core.User
{
    public static class UserSession
    {

        private static string firstLaunchDate;
        public static string FirstLaunchDate
        {
            get
            {
                if (firstLaunchDate == null)
                {
                    try
                    {
                        firstLaunchDate = GetStaticInstanceFomFileStore<string>("FirstLaunchDate");
                    }
                    catch (Exception ex)
                    {
                        //todo :Should never crash or show error. Think what to do with this error
                    }
                }
                if (firstLaunchDate != null)
                {
                    return firstLaunchDate;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                firstLaunchDate = value;
                try
                {
                    PersistStaticClassToFileStore<string>(value, "FirstLaunchDate");
                }
                catch (Exception ex)
                {
                    //todo :Should never crash or show error. Think what to do with this error
                }
            }
        }

        private static string lastUsedAppVersion;
        public static string LastUsedAppVersion
        {
            get
            {
                if (lastUsedAppVersion == null)
                {
                    try
                    {
                        lastUsedAppVersion = GetStaticInstanceFomFileStore<string>("LastUsedAppVersion");
                    }
                    catch (Exception ex)
                    {
                        //todo :Should never crash or show error. Think what to do with this error
                    }
                }
                if (lastUsedAppVersion != null)
                {
                    return lastUsedAppVersion;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                lastUsedAppVersion = value;
                try
                {
                    PersistStaticClassToFileStore<string>(value, "LastUsedAppVersion");
                }
                catch (Exception ex)
                {
                    //todo :Should never crash or show error. Think what to do with this error
                }
            }
        }
       
        private static UserProfile currentProfile;
        public static UserProfile Profile
        {
            get
            {
                if (currentProfile == null)
                {
                    try
                    {
                        currentProfile = GetStaticInstanceFomFileStore<UserProfile>();
                    }
                    catch (Exception ex)
                    {
                        //todo :Should never crash or show error. Think what to do with this error
                    }
                }
                if (currentProfile != null)
                {
                    return currentProfile;
                }
                else
                {
                    return new UserProfile();
                }               
            }
            set
            {
                currentProfile = value;
                try
                {
                    PersistStaticClassToFileStore<UserProfile>(value);
                }
                catch (Exception ex)
                {
                    //todo :Should never crash or show error. Think what to do with this error
                }
            }
        }

        private static Settings latestSettings;
        public static Settings Settings
        {
            get
            {
                if (latestSettings == null)
                {
                    try
                    {
                        latestSettings = GetStaticInstanceFomFileStore<Settings>();
                    }
                    catch (Exception ex)
                    {
                        //todo :Should never crash or show error. Think what to do with this error
                    }
                }
                if (latestSettings != null)
                {                  
                    return latestSettings;
                }
                else
                {
                    return new Settings();
                }
            }
            set
            {
                latestSettings = value;
                try
                {
                    PersistStaticClassToFileStore<Settings>(value);
                }
                catch (Exception ex)
                {
                    //todo :Should never crash or show error. Think what to do with this error
                }
            }
        }


        private static EmployeeProfile employeeProfile;
        public static EmployeeProfile EmployeeProfile
        {
            get
            {
                if (employeeProfile == null)
                {
                    try
                    {
                        employeeProfile = GetStaticInstanceFomFileStore<EmployeeProfile>();
                    }
                    catch (Exception ex)
                    {
                        //todo :Should never crash or show error. Think what to do with this error
                    }
                }
                if (employeeProfile != null)
                {
                    return employeeProfile;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                employeeProfile = value;
                try
                {
                    PersistStaticClassToFileStore<EmployeeProfile>(value);
                }
                catch (Exception ex)
                {
                    //todo :Should never crash or show error. Think what to do with this error
                }
            }
        }


        private static List<Schedule> latestSchedule;
        public static List<Schedule> LatestSchedule
        {
            get
            {
                if (latestSchedule == null)
                {
                    try
                    {
                        latestSchedule = GetStaticInstanceFomFileStore<List<Schedule>>();
                    }
                    catch (Exception ex)
                    {
                        //todo :Should never crash or show error. Think what to do with this error
                    }
                }
                if (latestSchedule != null)
                {
                    return latestSchedule;
                }
                else
                {
                    return new List<Schedule>();
                }
            }
            set
            {
                latestSchedule = value;
                try
                {
                    PersistStaticClassToFileStore<List<Schedule>>(value);
                }
                catch (Exception ex)
                {
                    //todo :Should never crash or show error. Think what to do with this error
                }
            }
        }
        public static List<Office> OfficeVenues
        {
            get;
            set;
        }
        
        public static void PersistStaticClassToFileStore<T>(object value, string name = null)
        {
            IMvxFileStore _fileStore = Mvx.Resolve<IMvxFileStore>();
            var filePath = _fileStore.NativePath("Profile");
            _fileStore.EnsureFolderExists("Profile");

            if (string.IsNullOrWhiteSpace(name))
                filePath = _fileStore.PathCombine("Profile", typeof(T) + ".txt");
            else
                filePath = _fileStore.PathCombine("Profile", name + "_" + typeof(T).Name + ".txt");

            if (value != null)
            {      
                var json = JsonConvert.SerializeObject(value);
                _fileStore.WriteFile(filePath, json);
            }
            else
            {
                _fileStore.DeleteFile(filePath);
            }
            
        }

        public  static T GetStaticInstanceFomFileStore<T>(string name = null)
        {
            IMvxFileStore _fileStore = Mvx.Resolve<IMvxFileStore>();
            string profileJsonText;
            var filePath = _fileStore.NativePath("Profile");
            _fileStore.EnsureFolderExists("Profile");

            if(string.IsNullOrWhiteSpace(name))
                filePath = _fileStore.PathCombine("Profile", typeof(T) + ".txt");
            else
                filePath = _fileStore.PathCombine("Profile", name+ "_" + typeof(T).Name + ".txt");

            if (_fileStore.TryReadTextFile(filePath, out profileJsonText))
            {

                if (!string.IsNullOrWhiteSpace(profileJsonText))
                {
                   return JsonConvert.DeserializeObject<T>(profileJsonText);
                }
            }
            return default(T);
        }

        public static string TempUUID {get;set;}


        static Random rm = new Random();
        private static readonly object syncLock = new object();
        public static string CreateUUID()
        {
            Regex rx = new Regex(@"[xy]");
            TempUUID =  rx.Replace("xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx", new MatchEvaluator(Replacer));
            InvalidateUser();
            return TempUUID;
        }
        static string Replacer(Match m)
        {
            var r = 0;
            lock (syncLock)
            {
                r = Convert.ToInt32(((decimal)rm.Next(1000000000, 1999999999) / (decimal)2000000000) * 16);
                var v = m.Value == "x" ? r : (r & 0x3 | 0x8);
                if (v == 16)
                {
                    v = 15;
                }
                return v.ToString("X");
            }
        }

        public static void InvalidateUser()
        {
            UserSession.Profile = null;
            UserSession.EmployeeProfile = null;
            UserSession.Settings = null;
        }




        
    }
}
