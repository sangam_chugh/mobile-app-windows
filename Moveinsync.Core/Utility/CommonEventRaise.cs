﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moveinsync.Core.ViewModels;
using Moveinsync.Core.EventArguments;
using Moveinsync.Core.Model;
using Moveinsync.Core.Analytics;

namespace Moveinsync.Core.Utility
{
    public static class CommonErrorEventRaiser
    {  
        public static void RaiseErrorEvent(object sender, EventHandler handler, Exception ex)
        {
            if (handler != null)
            {
                handler(sender, RaiseErrorEvent(ex));
            }
        }
        
        public static ErrorEventArgs RaiseErrorEvent(Exception ex)
        {
            if (ex is CustomError)
            {
                CustomError ce = (CustomError)ex;
                ErrorEventArgs errorargs;
                if (ce.Error.ApiError != null)
                {
                    //get the custom error code and add to the error list
                    if(string.IsNullOrWhiteSpace(ce.Error.ApiError.Message))
                        errorargs = new ErrorEventArgs(CustomErrorMessages.GetcustomErrorMessages(ce.Error.ApiError.Code), ex);
                    else
                        errorargs = new ErrorEventArgs(ce.Error.ApiError.Message, ex);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(ce.Error.ReasonPhrase))
                    {
                        errorargs = new ErrorEventArgs("Oops! Something went wrong" + "(" + ce.Error.ReasonPhrase.ToString() + ")", ex);
                    }
                    else
                    {
                        errorargs = new ErrorEventArgs("Oops! Something went wrong. Please try again later.", ex);
                    }
                   
                    
                }
                errorargs.IsNetworkConnectivityError = ce.IsConnectivityError;
                errorargs.IsAuthorizationError = ce.IsAuthorizationError;

                //var commonAlertEvent = new AnalyticEvent()
                //{
                //    EventName = FlurryConstants.Event.Alerts
                //};

                //LogAnalyticsEvent(commonAlertEvent);

                return errorargs;


            }
            else
            {
                return new ErrorEventArgs("Oops! Something went wrong", ex);
            }
        }
        
    
    }
}
