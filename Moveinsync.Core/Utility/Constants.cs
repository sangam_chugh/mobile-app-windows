﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Utility
{
    public class Constants
    {
        public class RequestHeader
        {
            public const string DefaultContentType = "application/json";
            public const string MIS_App_Version = "MIS_App_Version";
            public const string MIS_App_Version_Value = "2";
            public const string DefaultAcceptHeader = "*/*";
        }

        public class ExternalAnalytics
        {
            public const string CrittercismAppId = "5502b5f3e0697fa449637641";
            public const string FlurryApiKey = "5V96S3D822H9SWZBMD82"; 
        }

        public class MapSettings
        {
            public const bool EnableMapRegistration = true;
            public const string ApplicationId = "1308eca4-2584-4e7e-a836-24c949a40203"; //This  should come from app store                      
            public const string AuthenticationToken =  "3tmMXaPRJkfd9zhl5s97Dw"; //This  should come from app store
        }

        public class Service
        {
            //public const string BaseUri = "http://staging.moveinsync.com/mobile-app-server/";
            public const string BaseUri = "http://m.moveinsync.com/";           

            public class Employee
            {
                public const string GetProfile = "ets/user/profile";
            }

            public class Schedules
            {
                public const string GetSchedules = "ets/user/schedule";
                public const string GetOffices = "ets/user/schedule/office";
            }

            public class Registrations
            {
                public const string GetRegistered = "ets/user/register";
            }

            public class Shift
            {
                public const string GetShift = "ets/user/shifts ";
            }

            public class Validation
            {
                public const string GetValidate = "ets/user/register/validate";
            }

            public class TripDetail
            {
                public const string GetTripDetail = "ets/user/trip-details";
            }

            public class Settings
            {
                public const string GetSetings = "app/settings";
            }

            public class TripWithMilestone
            {
                public const string GetMilestoneTrip = "ets/cab/track-with-milestones"; 
            }

            public class Panic
            {
                public const string GetPanic = "ets/panic/raise"; 
            
            }

        }


       

        public class SettingTypes
        {
            public const string Profile = "profile";
            public const string TripDetails = "trip-details";
            public const string VehicleTracking = "vehicle-tracking";
            public const string Schedule = "schedule";
            public const string SOS = "sos";

        }

        public class ErrorCodes
        {
            public const int Code_0 = 0;
            public const int Code_1 = 1;
            public const int Code_1001 = 1001;
            public const int Code_1002 = 1002;
            public const int Code_1003 = 1003;
            public const int Code_1204 = 1204;
            public const int Code_1205 = 1205;
            public const int Code_5000 = 5000;
            public const int Code_5001 = 5001;
            public const int Code_5002 = 5002;
            public const int Code_5003 = 5003;
            public const int Code_5004 = 5004;
            public const int Code_5010 = 5010;
            public const int Code_5011 = 5011;
            public const int Code_6000 = 6000;
            public const int Code_6002 = 6002;
            public const int Code_6003 = 6003;
            public const int Code_6100 = 6100;
            public const int Code_6300 = 6300;

                
        }
    }
}
