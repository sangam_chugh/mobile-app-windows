﻿using Moveinsync.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.Utility
{
    public  class CustomErrorMessages
    {
        
        public static string GetcustomErrorMessages(int code)
        {
            string errorMssg = string.Empty;
            
            switch (code)
            {
                case 0: errorMssg = "Internet connectivity is not present.";
                    break;

                // HEADINGS: Poor Connectivity
                case 1: errorMssg = "Error Contacting Server. Please try again.";
                    break;
                //HEADINGS: Registration Error
                case 1001: errorMssg = "Request failed because device is not permitted to use transport service.";
                    break;
                //HEADINGS: Older Version
                case 1002: errorMssg = "You are using very old version of the application. Please update application to use transport service";
                    break;
                // HEADINGS: Limited Access
                case 1003: errorMssg = "You are not allowed to edit your schedule. Please contact your transport helpdesk for support";
                    break;
                //HEADINGS: Request Not Accepted
                case 1204: errorMssg = "Sorry, you are late to submit your request for changing your shift.Please contact your transport helpdesk";
                    break;
                //HEADINGS: Limited Access
                case 1205: errorMssg = "No Shifts are available";
                    break;

                case 5000:

                case 5001:

                case 5002:

                case 5003: errorMssg = "Request failed because device is not permitted to use transport service (rescheduling).";
                    break;
                //HEADINGS: Limited Access
                case 5004: errorMssg = "You are currently blocked from using mobile application. Please contact your transport team";
                    break;
                //HEADINGS: Validation Failed,
                case 5010: errorMssg = "One Time Passcode validation failed. Please try again";
                    break;
                case 5011: errorMssg = "One Time Passcode (OTP) is expired. Please request for OTP again";
                    break;
                //HEADINGS: Number Not Registered.
                case 6000: errorMssg = "The phone number is not a registered phone to use transport service";
                    break;
                //HEADINGS: Limited Access
                case 6002: errorMssg = "You can see only next 7 days of your schedule";
                    break;
                //HEADINGS: Tripsheet Pending.
                case 6003: errorMssg = "Your trip is not created yet";
                    break;
                // HEADINGS: Tripsheet Pending
                case 6100: errorMssg = "Tracking info unavailable. Trying again..";
                    break;
                //HEADINGS: Limited Access
                case 6300: errorMssg = "Request failed because device is not permitted to use transport service.";
                    break;
                default: 
                    errorMssg = "Oops! Something went wrong. Please try again after some time.";
                    break;
            }

            return errorMssg;
        }
    }


    //public static class Alerts
    //{
    //  public  static string REGISTRATION_SERVICE_SUCCESS_MESSAGE = "One time passcode sent over SMS for validation. Enter the passcode to complete registation.";


    //  public static string REGISTRATION_SERVICE_FAILURE_MESSAGE = "Sorry. Your Phone number is not registered with MoveInSync.";

    //  public static string OTP_REQUEST_SUCCESS_MESSAGE = "One Time Password is sent as SMS to complete registration";



    //  public static string PASSWORD_VALIDATION_SERVICE_SUCCESS_MESSAGE = "Registration successful, contact your TR.DESK to activate other features.";


    //  public static string PASSWORD_VALIDATION_SERVICE_FAILURE_MESSAGE = "Sorry. You seem to have entered the wrong OTP.";

    //  public static string REQUEST_OTP_SERVICE_SUCCESS_MESSAGE = "Request for OTP successful. You shall receive a new OTP";

    //  public static string REQUEST_OTP_SERVICE_FAILURE_MESSAGE = "Sorry.OTP cannot be generated again";

    //  public static string PROFILE_SERVICE_SUCCESS_MESSAGE = "Profile info received";

    //  public static string PROFILE_SERVICE_FAILURE_MESSAGE = "Sorry.Cannot retrieve profile info";
    //  public static string PANIC_SERVICE_SUCCESS_MESSAGE = "SOS alert raised!!!";

    //  public static string PANIC_SERVICE_FAILURE_MESSAGE = "Unable to flash SOS via this app. due to mobile network connectivity. Please try again";

    //  public static string PANIC_SERVICE_SUCCESS_MESSAGE_WITHOUT_LOCATION = "SOS message flashed. However your location is not sent with SOS";

    //  public static string INTERNET_CONNECTIVITY_NOT_PRESENT_MESSAGE = "Internet connectivity is not present.";
    //  public static string INVALID_PHONE_NUMBER = "Invalid Phone number. Please enter a 10 digit number";

    //  public static string INVALID_PASSWORD = "Please enter the one time password";



    //  public static string NO_CAB_ASSOCIATED_DATA = "No Cab assigned to trip";



    //  public static string UNEXPECTED_ERROR = "Some unexpected error has occured";



    //  public static string TRACKING_FAILED = "Unable to get cab location at ";



    //    //                         HomeManager.js:

    //    //167 var errorMssg = "Unable to configure app. Internet is required for the app to function 

    //    //properly. Please check connectivity and try again.";

    //    //HEADINGS: App Configuration Failed.

    //    //27: var errorText= 'Past schedule cannot be viewed';

    //    //HEADINGS: Past schedule cannot be viewed

    //    //83: var errorText = "Sorry, you are late to submit your request for changing your shift. 

    //    //Please contact your transport helpdesk.";

    //    //HEADINGS: Late Request.

    //    //104: var errorText = "It seems your logout will be past midnight. If so, please schedule your 

    //    //logout for next day"; 

    //    //HEADINGS: Reschedule your Logout

    //    //117: var errorText = "You can see only next "+ scheduleDaysize + " days of your schedule"; 

    //    //HEADINGS: Notification

    //    //TrackingManager.js:

    //    //263  var errorMssg = “AlertMessages.TRACKING_FAILED + dateUtils.getTimeWithSeconds(date) + ". 

    //    //Retrying...";

    //    //HEADINGS:Tracking Failed

    //}

    }


