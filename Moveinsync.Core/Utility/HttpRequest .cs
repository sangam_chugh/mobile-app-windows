﻿using Moveinsync.Core.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Moveinsync.Core.Model;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.CrossCore;
using Moveinsync.Core.Messages;

namespace Moveinsync.Core.Utility
{
    /// <summary>
    /// Common HTTP Request Class
    /// Supports GET/POST methods
    /// Supports JSON and XML Serialization 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class HttpRequest<T>
    {
        private string uri = string.Empty;
        private RequestMethod requestMethod = RequestMethod.GET;
        private Dictionary<string, string> customHeaders = null;
        private Dictionary<string, string> urlParams = null;
        private bool enableCache = false;
        private HttpContent httpContent = null;
        private Dictionary<string, string> cookies = null;
        private string UUID { get; set; }



        /// <summary>
        /// Initialize a HTTPRequest helper. Works for both GET & POST methods
        /// </summary>
        /// <param name="uri">The web serive URL</param>
        /// <param name="requestMethod">GET/POST</param>
        /// <param name="urlParams">any query string parameters</param>
        /// <param name="cookies">cookies to be sent across</param>
        /// <param name="enableCache">Can we use server cache or request new?</param>
        /// <param name="content"></param>
        public HttpRequest(string uri, RequestMethod requestMethod, Dictionary<string, string> customHeaders = null, Dictionary<string, string> urlParams = null, Dictionary<string, string> cookies = null, bool enableCache = false, HttpContent content = null, string uuid = null)
        {
            this.uri = uri;
            this.requestMethod = requestMethod;
            this.customHeaders = customHeaders ?? new Dictionary<string, string>();
            this.urlParams = urlParams ?? new Dictionary<string, string>();
            this.enableCache = enableCache;
            this.httpContent = content;
            this.cookies = cookies ?? new Dictionary<string, string>();
            this.UUID = uuid;
        }

        /// <summary>
        /// Deserialize a request 
        /// Read JSON and populate object
        /// </summary>
        /// <returns>Object deserialzied</returns>

        public async Task<T> GetResponseForJSON()
        {

            string responseString = await GetResponseString();
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseString);
        }

        /// <summary>
        /// Private function - called to make the necessary HTTP requests
        /// </summary>
        /// 
        /// <returns>String received in the response</returns>

        public async Task<string> GetResponseString()
        {
            try
            {
                CookieContainer a = new CookieContainer();
                HttpClientHandler handler = new HttpClientHandler()
                {
                    CookieContainer = a
                };

                HttpClient httpClient = new HttpClient(handler);
                httpClient.Timeout = TimeSpan.FromMinutes(1);

                // Add Custom headers to the request
                foreach (var key in customHeaders.Keys)
                {
                    httpClient.DefaultRequestHeaders.Add(key, customHeaders[key]);
                }

                // Add Cookies to the request
                foreach (var key in cookies.Keys)
                {
                    a.Add(new Uri(uri), new Cookie(key, cookies[key]));
                }


                if (!enableCache)
                {

                    urlParams.Add("nckey", Guid.NewGuid().ToString());
                }

                string requestUri = uri;
                // Add URL Parameters to the request

                if (urlParams.Count > 0)
                {

                    requestUri = string.Concat(requestUri, "?", string.Join("&", urlParams.Select(c => string.Concat(c.Key, "=", c.Value))));
                }


                if (!string.IsNullOrEmpty(this.UUID))
                {
                    httpClient.DefaultRequestHeaders.Add("Authorization", this.UUID);
                    if (this.httpContent != null)
                    {
                        this.httpContent.Headers.ContentType = new MediaTypeHeaderValue(Constants.RequestHeader.DefaultContentType);
                    }
                    httpClient.DefaultRequestHeaders.Add(Constants.RequestHeader.MIS_App_Version, Constants.RequestHeader.MIS_App_Version_Value);
                    httpClient.DefaultRequestHeaders.Add("Accept", Constants.RequestHeader.DefaultAcceptHeader);

                }
                else
                {
                    //Send message using mesenger to send user to login page
                    //throw new Exception("No access token found. Redirect to login page");
                }

                HttpResponseMessage response;
                string responseString = string.Empty;
                try
                {
                    if (requestMethod == RequestMethod.GET)
                    {
                        response = await httpClient.GetAsync(requestUri);
                    }
                    else if (requestMethod == RequestMethod.PUT)
                    {

                        var request = new HttpRequestMessage(HttpMethod.Put, requestUri);
                        request.Content = httpContent;
                        response = await httpClient.SendAsync(request);
                    }
                    else
                    {

                        var request = new HttpRequestMessage(HttpMethod.Post, requestUri);
                        request.Content = httpContent;
                        response = await httpClient.SendAsync(request);

                    }
                    responseString = await response.Content.ReadAsStringAsync();
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        return responseString;
                    }

                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        //The user is using an invalid authorization code

                        CustomError error = ExtractErrorCodeMessage(response, responseString);
                        error.IsAuthorizationError = true;

                        IMvxMessenger _messenger = Mvx.Resolve<IMvxMessenger>();
                        _messenger.Publish(new InvalidSessionMessage(this, error));

                        throw error;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Forbidden
                             || response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    {
                        //Some server side error
                        throw ExtractErrorCodeMessage(response, responseString);
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        //Unalbe to contact server
                        var error = ExtractErrorCodeMessage(response, responseString);
                        error.IsConnectivityError = true;
                        throw error;
                    }
                    else
                    {
                        //Other unhandled errors
                        var customerError = ExtractErrorCodeMessage(response, responseString);

                        if (customerError.Error != null && customerError.Error.ApiError != null && customerError.Error.ApiError.Code == 6300)
                        {
                            customerError.IsAuthorizationError = true;
                            IMvxMessenger _messenger = Mvx.Resolve<IMvxMessenger>();
                            _messenger.Publish(new InvalidSessionMessage(this, customerError));
                            throw customerError;
                        }
                        else
                        {
                            throw customerError;
                        }
                    }
                }
                catch (CustomError ce)
                {
                    throw ce;//Todo : Send custom exception

                }

            }
            catch (CustomError ce)
            {
                throw ce;//Todo : Send custom exception
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace);
                throw;//Todo : Send custom exception
            }
        }

        private static CustomError ExtractErrorCodeMessage(HttpResponseMessage response, string responseString)
        {
            try
            {
                CustomError ObCustomsErrors = new CustomError()
                {
                    Error = new ErrorModel()
                    {
                        ApiError = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiErrorModel>(responseString),
                        HttpStatusMessage = response.StatusCode.ToString(),
                        ReasonPhrase = response.ReasonPhrase
                    }
                };
                return ObCustomsErrors;
            }
            catch (Newtonsoft.Json.JsonException e)
            {
                CustomError ObCustomsErrors = new CustomError()
                {
                    Error = new ErrorModel()
                    {
                        ApiError = null,
                        HttpStatusMessage = response.StatusCode.ToString(),
                        ReasonPhrase = response.ReasonPhrase
                    }
                };
                return ObCustomsErrors;
            }
        }

    }

    public enum RequestMethod
    {
        GET,
        POST,
        PUT
    }
}