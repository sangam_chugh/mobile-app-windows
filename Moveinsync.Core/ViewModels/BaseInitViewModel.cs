﻿using Cirrious.CrossCore;
using Cirrious.CrossCore.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moveinsync.Core.ViewModels
{
    public abstract class BaseInitViewModel<TInit>
      : BaseViewModel
    {
        public void Init(string parameter)
        {
            var deserialized = Mvx.Resolve<IMvxJsonConverter>().DeserializeObject<TInit>(parameter);
            ComplexInit(deserialized);
        }

        protected abstract void ComplexInit(TInit parameter);

    }

}
