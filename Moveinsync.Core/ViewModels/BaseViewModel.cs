﻿
using Cirrious.CrossCore;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using Moveinsync.Core.EventArguments;
using Moveinsync.Core.Analytics;
using Moveinsync.Core.Messages;
using Moveinsync.Core.Model;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using Moveinsync.Plugins.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimerReference.Core;

namespace Moveinsync.Core.ViewModels
{
    public class BaseViewModel : MvxViewModel
    {
        public bool HasNetworkConnection { get; private set; }
        public bool HasInternetConnection { get; private set; }

        private INetworkService networkService = null;

        public BaseViewModel()
        {

            networkService = Mvx.Resolve<INetworkService>();
            HasNetworkConnection = networkService.IsConnected;
            HasInternetConnection = networkService.IsConnected; //Default it true if connected to any network;
            networkService.Subscribe(new Action<NetworkStatusChangedMessage>(NetworkStatusChanged));

            UpdateNetworkConnectivity();
        }

        protected void LogAndLogoutUser(string errorMessage)
        {
            UserSession.InvalidateUser();

            this.ShowViewModel(typeof(LoginViewModel), new { errorMessage = errorMessage });

        }
        private async Task UpdateNetworkConnectivity()
        {
            HasInternetConnection = await networkService.IsHostReachable("google.com");
        }

        public async Task<bool> GetLatestInternetConnectivity()
        {
            await UpdateNetworkConnectivity();
            return HasInternetConnection;
        }

        private void NetworkStatusChanged(NetworkStatusChangedMessage obj)
        {
            HasNetworkConnection = obj.Status.IsConnected;
            if (!HasNetworkConnection)
            {
                HasInternetConnection = HasNetworkConnection;
            }
        }

        public virtual void OnResume()
        {
            ScheduleGenericTimer();
        }

        public virtual void OnPause()
        {
            KillGenericTimer();
        }

        private PCLTimer genericTimerInstance = null;
        private void ScheduleGenericTimer()
        {

            if (genericTimerInstance == null)
            {
                //set up timer to run every second
                genericTimerInstance = new PCLTimer(new Action(CommonPolling), TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(5));
            }
            //timer starts one second from now
            genericTimerInstance.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(5));

        }

        private void CommonPolling()
        {
            if (!HasInternetConnection)
            {
                UpdateNetworkConnectivity();
            }
        }

        private void KillGenericTimer()
        {
            if (genericTimerInstance != null)
            {
                genericTimerInstance.Dispose();
                genericTimerInstance = null;
            }
        }


        private const string ParameterName = "parameter";

        public EventHandler EvntThrowError;
        public void ThrowError(ErrorEventArgs args)
        {
            LogError(args);
            if (EvntThrowError != null)
                EvntThrowError(this, args);
        }

        protected void LogError(ErrorEventArgs args)
        {
            if (args.IsNetworkConnectivityError)
            {
                UpdateNetworkConnectivity();
            }

            if (args.IsAuthorizationError)
            {
                LogAndLogoutUser(args.Errors.FirstOrDefault());
            }
            //Error is passed to UI for logging in Crtittercism using UI SDK
            LogErrorInUI(args);
        }

        public EventHandler EvntLogError;
        private void LogErrorInUI(ErrorEventArgs args)
        {
            if (EvntLogError != null)
            {
                EvntLogError(this, args);
            }
        }

        protected void ShowComplexViewModel(Type viewModelType, object parameter)
        {
            var text = Mvx.Resolve<IMvxJsonConverter>().SerializeObject(parameter);
            base.ShowViewModel(viewModelType, new Dictionary<string, string>()
           {
               {ParameterName, text}
           });
        }

        public EventHandler EvntLogAnalytics;
        protected void LogAnalyticsEvent(AnalyticEvent log)
        {
            if(EvntLogAnalytics != null)
            {
                EvntLogAnalytics(this, new AnalyticEventArgs(log));
            }
        }
    }
}
