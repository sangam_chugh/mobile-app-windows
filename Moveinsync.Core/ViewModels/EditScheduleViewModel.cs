﻿using Cirrious.MvvmCross.ViewModels;
using Moveinsync.Core.EventArguments;
using Moveinsync.Core.IServices;
using Moveinsync.Core.Model;
using Moveinsync.Core.Services;
using Moveinsync.Core.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Moveinsync.Core.ViewModels
{
    public class EditScheduleViewModel : BaseInitViewModel<Moveinsync.Core.ViewModels.EditScheduleViewModel.ScheduleInput>
    {
        public class ScheduleInput
        {
            public Schedule Schedule { get; set; }
            public bool IsLogin { get; set; }
        }


        public IShiftService _shiftService;
        private IScheduleService scheduleService;
        public EditScheduleViewModel(IShiftService shiftService, IScheduleService scheduleService)
        {
            this._shiftService = shiftService;
            this.scheduleService = scheduleService;
        }

        public Schedule Schedule { get; set; }
        public ScheduleItem ScheduleItem { get; set; }
        public bool isLogin { get; set; }

        public ObservableCollection<Shift> Shifts { get; set; }

        public string ShiftValueDisplay
        {
            get;
            set;
        }

        public EventHandler ShiftWarning;

        public void CallLoginShiftWarning()
        {
            try
            {                
                ThrowError(new ErrorEventArgs("Sorry, you are too late to submit your request for changing your shift. Please contact your transport helpdesk"));
                Close(this);
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }


        private Shift selectedShift { get; set; }
        public Shift SelectedShift
        {
            get
            {
                return selectedShift;
            }
            set
            {
                selectedShift = value;
                ShiftChanged();
            }
        }
        //PostSchedule

        private IMvxCommand _postSchedule;

        public IMvxCommand PostSchedule
        {
            get
            {
                _postSchedule = _postSchedule ?? new MvxCommand(SaveNewShift);
                return _postSchedule;
            }

        }

        public string PickupAddress { get; set; }
        public string DropAddress { get; set; }

        private void ShiftChanged()
        {
            try
            {
                ShiftValueDisplay = SelectedShift.Display;
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        public bool GettingDataFromService { get; set; }

        protected override async void ComplexInit(ScheduleInput item)
        {
            GettingDataFromService = true;
            try
            {
                Schedule = item.Schedule;
                ScheduleItem = item.IsLogin ? item.Schedule.Login : item.Schedule.Logout;
               
                if (item.IsLogin)
                {
                    PickupAddress = item.Schedule.Login.PickupAddress;
                    DropAddress = item.Schedule.Login.DropAddress;
                }
                else
                {
                    PickupAddress = item.Schedule.Logout.PickupAddress;
                    DropAddress = item.Schedule.Logout.DropAddress;
                }


                isLogin = item.IsLogin;

                Shifts dayShifts = await _shiftService.GetShifts(Schedule.DateValue);
                if (isLogin)
                {
                    Shifts = new ObservableCollection<Shift>(dayShifts.LoginShifts);
                }
                else
                {
                    Shifts = new ObservableCollection<Shift>(dayShifts.LogoutShifts);
                }

                Shift leaveItem;
                Shift notScheduledItem = null;

                foreach (var shift in Shifts)
                {
                    if (shift.Hours == -1)
                    {
                        notScheduledItem = shift;
                    }
                    if (shift.Hours == -2)
                    {
                        leaveItem = shift;
                    }
                    if (shift.Hours == ScheduleItem.Hours && shift.Minutes == ScheduleItem.Minutes)
                    {
                        SelectedShift = shift;
                    }
                }

                if (SelectedShift == null)
                {
                    if (notScheduledItem != null)
                    {
                        SelectedShift = notScheduledItem;
                    }

                }

                if (SelectedShift == null)
                {

                    if (string.Equals(ScheduleItem.State, "Cancelled", StringComparison.CurrentCultureIgnoreCase))
                    {
                        ShiftValueDisplay = "Cancelled";
                    }
                    else
                    {
                        ShiftValueDisplay = "Not Scheduled";
                    }
                }
                else
                {
                    ShiftValueDisplay = SelectedShift.Display;
                }

                if (string.Equals(ScheduleItem.State, "scheduled", StringComparison.CurrentCultureIgnoreCase) && ScheduleItem.Hours >=0)
                { // enter edit page
                    var today = DateTime.Now.Date;
                    var scheduleDate = Convert.ToDateTime(item.Schedule.Date);
                    if (scheduleDate <= today)
                    {
                        if (today == scheduleDate)
                        {
                            if (!Shifts.Any(m => (m.Hours == ScheduleItem.Hours && m.Minutes==ScheduleItem.Minutes)))
                            {
                                //exit out of edit page with message
                                CallLoginShiftWarning();
                            }
                        }
                        else
                        {
                            //Cannot edit previous date
                            CallLoginShiftWarning();
                        }
                    }                   
                }
            }
            catch (Exception ex)
            {
                ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
                Close(this);
            }
            GettingDataFromService = false;
        }
        public EventHandler ScheduledPostEvent;
        // public EventHandler 


        public async void SaveNewShift()
        {
            GettingDataFromService = true;
            try
            {
                string flag = await scheduleService.UpdateScheduleItem(Schedule, SelectedShift);

                if (flag == null || flag == "")
                {
                    ScheduleArgs args = new ScheduleArgs();
                    if (selectedShift.Type.Equals("login", StringComparison.CurrentCultureIgnoreCase) && selectedShift.Hours >= 16)
                    {
                        args.Message = "success4pm";
                    }
                    else
                    {
                        args.Message = "success";
                    }
                    
                    if (ScheduledPostEvent != null)
                    {
                        ScheduledPostEvent(this, args);
                    }
                    ShowViewModel<HomeViewModel>();
                }
                else
                {
                    ScheduleArgs args = new ScheduleArgs();
                    args.Message = "Not Saved";
                    if (ScheduledPostEvent != null)
                    {
                        ScheduledPostEvent(this, args);
                    }

                }

            }
            catch (Exception ex)
            {
                ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
                //Close(this);
            }

            GettingDataFromService = false;

        }

        private IMvxCommand _dogoBackToSchedule;

        public IMvxCommand DoGoBackToSchedule
        {
            get
            {
                _dogoBackToSchedule = _dogoBackToSchedule ?? new MvxCommand(GoBackToSchedule);
                return _dogoBackToSchedule;
            }

        }

        public void GoBackToSchedule()
        {
            try
            {
                Close(this);
            }
            catch (Exception ex)
            {
                //some message to show
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }
    }
}
