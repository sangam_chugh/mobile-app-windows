﻿using Cirrious.CrossCore;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.Plugins.File;
using Cirrious.MvvmCross.Plugins.Location;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using Moveinsync.Core.Analytics;
using Moveinsync.Core.EventArguments;
using Moveinsync.Core.IService;
using Moveinsync.Core.IServices;
using Moveinsync.Core.Messages;
using Moveinsync.Core.Model;
using Moveinsync.Core.Resources;
using Moveinsync.Core.Services;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TimerReference.Core;


namespace Moveinsync.Core.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        #region constructor

        private readonly IEmployeeProfileService _employeeProfileService;
        private readonly IScheduleService _scheduleService;
        private readonly IOfficeAddressService _officeService;
        private readonly ISettingsService _settingsService;
        private readonly IPanicService _panicService;
        private readonly ITrackWithMilestoneService _trackWithMilestoneService;
        private readonly ITripDetailsService _tripDetailsService;
        private readonly IMvxLocationWatcher _watcher;


        private IMvxMessenger _messenger = null;
        public HomeViewModel(IMvxLocationWatcher watcher, IEmployeeProfileService employeeService, IScheduleService scheduleService,
            IOfficeAddressService officeService, IPanicService panicService, ISettingsService settingsService, ITrackWithMilestoneService trackWithMilestoneService,
            ITripDetailsService tripDetailsService)
        {
            _employeeProfileService = employeeService;
            _scheduleService = scheduleService;
            _settingsService = settingsService;
            _panicService = panicService;
            _trackWithMilestoneService = trackWithMilestoneService;
            _tripDetailsService = tripDetailsService;
            _watcher = watcher;

            _officeService = officeService;
            _messenger = Mvx.Resolve<IMvxMessenger>();
            _messenger.Subscribe<InvalidSessionMessage>(LogoutUser);
            ScheduleNotFoundMessage = "getting schedule";
            RouteListPointCount = 0;

        }
        protected void LogoutUser(InvalidSessionMessage obj)
        {
            try
            {
                LogAndLogoutUser(CommonErrorEventRaiser.RaiseErrorEvent(obj.Error).Errors.FirstOrDefault());
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }



        #endregion

        public override void OnResume()
        {
            base.OnResume();

            //if(_watcher != null)
            //    _watcher.Start(new MvxLocationOptions(), OnLocation, OnError);
            try
            {
                PopulateAll();
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        public override void OnPause()
        {
            base.OnPause();

            //if (_watcher != null && _watcher.Started)
            //    _watcher.Stop();
            try
            {
                KillTimer();
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        public async void PopulateAll()
        {
            try
            {
                if (await GetSettings())
                {

                    GetLatestProfile();
                    ReloadScheduleTimer();
                    ReloadtrackCabTimer();
                    KillSettingsTimer();
                }
                else
                {
                    ReloadSettingsTimer();
                }

            }
            catch (Exception ex)
            {
                ReloadSettingsTimer();
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        public List<Office> OfficeVenues { get; set; }
        private async Task GetOfficeList()
        {
            try
            {
                UserSession.OfficeVenues = OfficeVenues = await _officeService.GetOffices();
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }

        }

        public List<TrackMilestone> currentTripMilestones = new List<TrackMilestone>();
        DateTime latestMilestoneUpdateTime;

        public string TripPickupAddress
        {
            get
            {
                if (TripDetails != null && TripDetails.Pickup != null)
                {
                    if (Profile != null && string.Equals(Profile.Address, TripDetails.Pickup.Address, StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (!string.IsNullOrWhiteSpace(TripDetails.Pickup.Landmark))
                            return TripDetails.Pickup.Landmark;
                    }
                    return TripDetails.Pickup.Address;
                }
                return string.Empty;
            }
        }

        public string TripDropAddress
        {
            get
            {
                if (TripDetails != null && TripDetails.Drop != null)
                {
                    if (Profile != null && string.Equals(Profile.Address, TripDetails.Drop.Address, StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (!string.IsNullOrWhiteSpace(TripDetails.Drop.Landmark))
                            return TripDetails.Drop.Landmark;
                    }
                    return TripDetails.Drop.Address;
                }
                return string.Empty;
            }
        }

        public bool HasTripBeenPlotted
        {
            get;
            set;
        }

        public bool TripOrSOSAvailable
        {
            get
            {
                return TripDetailsIsAvailable || (Settings != null && Settings.IsSOSEnabled);
            }

        }

        public async Task GetTripDetails()
        {

            string lastSeenTime = string.Empty;
            try
            {
                Trip newTrip = null;
                try
                {
                    newTrip = await _tripDetailsService.GetTripDetais();
                }
                catch (Exception ex)
                {
                    //No trip scenarios or actual eror
                    LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
                }


                if (TripDetails == null && newTrip != null)
                {
                    EventGotoTrackPage(this, new EventArgs()); // A new trip was started for user. So take him to that panorama
                }

                if (newTrip == null) // A trip has ended or no trips assigned
                {
                    TripDetails = null;
                    currentTripMilestones = new List<TrackMilestone>();
                    TripDetailsIsAvailable = false; // Remove all trip details from map
                    TrackingIsAvailable = false; //Remove tracking details from map
                    HasTripBeenPlotted = false;
                    EventPlotMap(this, new MapArguments()); // clear map
                    TrackCab = null; // clear Cab milestone Details

                }
                else
                {
                    if (TripDetails != null && TripDetails.Pickup.DateTime != newTrip.Pickup.DateTime) // if dateime is not same it is a new trip
                    {
                        currentTripMilestones = new List<TrackMilestone>(); //clear existing milestones
                    }
                    TripDetails = newTrip;

                    if (TripDetails.Cab != null)
                    {
                        TripDetailsIsAvailable = true;
                        var newTrackMilestone = await _trackWithMilestoneService.GetTrackWithMilestones(TripDetails.Cab.vehicleRegNumber, TripDetails.Cab.misVehicleId);

                        if (newTrackMilestone == null)
                        {
                            //Don't have tracking data
                            TrackCab = null;
                            TrackingIsAvailable = false;
                            HasTripBeenPlotted = false;
                            EventPlotMap(this, new MapArguments());

                        }
                        else
                        {
                            if (currentTripMilestones.Count > 0)
                            {
                                HasTripBeenPlotted = true;
                            }

                            if (!(TrackCab != null && newTrackMilestone.Time == TrackCab.Time)) //Milestone has changed/updated
                            {
                                latestMilestoneUpdateTime = DateTime.Now;
                                currentTripMilestones.Add(newTrackMilestone);
                                ReloadtrackCabTimer();
                            }

                            lastSeenTime = string.Format("{0} mins ago", Math.Round((DateTime.Now - latestMilestoneUpdateTime).TotalMinutes, 0));


                            MapArguments args = new MapArguments()
                            {
                                LatestUpdatedTime = UnixTimeStamp.ConvertFromUnixTimestamp(newTrackMilestone.Time / 1000),
                                LatestBearing = newTrackMilestone.Bearing,
                                LatestLatitude = newTrackMilestone.Latitude,
                                LatestLongitude = newTrackMilestone.Longitude,
                                LatestSpeed = newTrackMilestone.Speed,
                                LastSeentime = lastSeenTime
                            };

                            TrackCab = newTrackMilestone;
                            TrackingIsAvailable = true;
                            EventPlotMap(this, args);

                        }
                    }
                    else
                    {
                        TripDetailsIsAvailable = false;
                        TrackCab = null;
                        TrackingIsAvailable = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }
        public void NavigateToPrivacy()
        {
            try
            {
                if (HasInternetConnection)
                {
                    ShowViewModel<PrivacyPolicyViewModel>();
                    AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                    objAnalyticEvent.EventName = FlurryConstants.Event.PrivacyPolicy_Visit;

                    LogAnalyticsEvent(objAnalyticEvent);
                }
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }

        }
        public void NavigateToTermsAndConditions()
        {
            try
            {
                if (HasInternetConnection)
                {
                    ShowViewModel<TermsAndConditionsViewModel>();
                    AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                    objAnalyticEvent.EventName = FlurryConstants.Event.TermsAndconditions_Visit;

                    LogAnalyticsEvent(objAnalyticEvent);
                }
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }


        public int GetRetryTime(int locationsLength)
        {

            var retryTime = locationsLength * Settings.VehicleTracking.MapMilestoneDrawTime;

            if (retryTime < Settings.VehicleTracking.RetryTimeLowerLimit)
                retryTime = Settings.VehicleTracking.RetryTimeLowerLimit;
            else if (retryTime > Settings.VehicleTracking.RetryTimeUpperLimit)
                retryTime = Settings.VehicleTracking.RetryTimeUpperLimit;

            return retryTime;

        }


        #region Data Members

        public bool ProfileIsAvailable { get; set; }
        public bool TrackingIsAvailable { get; set; }
        //public bool CabIsAssigned { get; set; }
        public bool TripDetailsIsAvailable { get; set; }
        public bool ScheduleIsAvailable { get; set; }
        public string ScheduleStatus { get; set; }
        public bool SettingsIsAvailable { get; set; }
        public bool GettingProfile { get; set; }
        public bool GettingSchedule { get; set; }

        private ObservableCollection<Schedule> schedules;
        public ObservableCollection<Schedule> Schedules
        {
            get
            {
                if (schedules == null)
                {
                    schedules = new ObservableCollection<Schedule>(UserSession.LatestSchedule);
                }
                return schedules;
            }
            set
            {
                schedules = value;
                UserSession.LatestSchedule = schedules.ToList();
            }
        }
        private Settings _settings;
        public Settings Settings
        {

            get
            {
                return _settings;
            }

            set
            {
                _settings = value;
                SettingsChanged(new EventArgs());
                RaisePropertyChanged(() => Settings);
            }

        }
        private TrackMilestone _tracks;
        public TrackMilestone Tracks
        {
            get { return _tracks; }

            set { _tracks = value; RaisePropertyChanged(() => Tracks); }

        }

        public bool ShowSOSButton
        {
            get
            {
                return Settings != null && Settings.IsSOSEnabled && Settings.LongPressDuration.HasValue;
            }
        }

        public string ScheduleNotFoundMessage { get; set; }




        #endregion

        #region timer

        private PCLTimer scheduleTimerInstance = null;
        private PCLTimer trackCabTimerInstance = null;
        private PCLTimer settingsTimerTimerInstance = null;

        private void ReloadScheduleTimer()
        {
            if (UserSession.Settings.IsProfileEnabled)
            {
                if (scheduleTimerInstance == null)
                {
                    //set up timer to run every second
                    scheduleTimerInstance = new PCLTimer(new Action(GetLatestScheduleAction), TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(240));
                }
                //timer starts one second from now
                scheduleTimerInstance.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(240));
            }
        }

        private void ReloadSettingsTimer()
        {
            if (settingsTimerTimerInstance == null)
            {
                //set up timer to run every second
                settingsTimerTimerInstance = new PCLTimer(new Action(PopulateAll), TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(120));
            }
            //timer starts one second from now
            settingsTimerTimerInstance.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(120));

        }
        DateTime ScheduleStartTime;
        private void GetLatestScheduleAction()
        {
            ScheduleStartTime = DateTime.Now;
            Task.Run(() => GetLatestSchedule());

        }


        private int trackCabTimerSpanSeconds
        {
            get
            {
                return Settings != null ? Settings.IsVehicleTrackingEnabled ? GetRetryTime(RouteListPointCount) : 0 : 0;
            }
        }

        public int RouteListPointCount { get; set; }

        private void ReloadtrackCabTimer()
        {
            if (UserSession.Settings.IsTripDetailsEnabled)
            {
                if (trackCabTimerInstance == null)
                {

                    trackCabTimerInstance = new PCLTimer(new Action(TrackCabAction), TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(trackCabTimerSpanSeconds));
                }
                trackCabTimerInstance.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(trackCabTimerSpanSeconds));
            }
        }
        DateTime TrackStartTime;
        private void TrackCabAction()
        {
            TrackStartTime = DateTime.Now;
            Task.Run(() => GetTrackCabDetails());
        }

        private void KillTimer()
        {
            if (scheduleTimerInstance != null)
            {
                scheduleTimerInstance.Dispose();
                scheduleTimerInstance = null;
            }

            if (trackCabTimerInstance != null)
            {
                trackCabTimerInstance.Dispose();
                trackCabTimerInstance = null;
            }

            KillSettingsTimer();
        }

        private void KillSettingsTimer()
        {
            if (settingsTimerTimerInstance != null)
            {
                settingsTimerTimerInstance.Dispose();
                settingsTimerTimerInstance = null;
            }
        }

        #endregion

        #region Get Data Methods

        private async Task<bool> GetSettings()
        {
            try
            {
                UserSession.Settings = await _settingsService.GetSettings();
                Settings = UserSession.Settings;
                SettingsIsAvailable = true;
                return true;
            }
            catch (Exception ce)
            {
                if (UserSession.Settings != null)
                {
                    Settings = UserSession.Settings;
                }
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ce));

                SettingsIsAvailable = false;
            }
            return false;
        }

        private async Task GetLatestProfile()
        {

            try
            {

                if (UserSession.Settings.IsProfileEnabled)
                {
                    var startTime = DateTime.Now;

                    ProfileIsAvailable = true;
                    GettingProfile = true;

                    Profile = await _employeeProfileService.GetProfile();

                    UserSession.EmployeeProfile = Profile;

                    if (Profile != null)
                    {
                        AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                        objAnalyticEvent.EventName = FlurryConstants.Event.ProfileLoaded;
                        objAnalyticEvent.Parameters.Add(new KeyValuePair<string, string>("EmployeeId", Profile.EmployeeId));
                        objAnalyticEvent.Parameters.Add(new KeyValuePair<string, string>("Organization", Profile.Organization));
                        LogAnalyticsEvent(objAnalyticEvent);
                    }

                    var ProfileLoadEvent = new AnalyticEvent()
                    {
                        EventName = FlurryConstants.Event.ProfilePanorma_LoadTime
                    };
                    double loadTime = (DateTime.Now - startTime).TotalMilliseconds;
                    ProfileLoadEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.LoadTime_TimeSpanInMilliSeconds, loadTime.ToString()));
                    LogAnalyticsEvent(ProfileLoadEvent);
                }
            }

            catch (Exception ce)
            {
                if (UserSession.EmployeeProfile != null)
                {
                    Profile = UserSession.EmployeeProfile;
                    ProfileIsAvailable = true;


                }
                else
                {
                    ProfileIsAvailable = false;
                }

                CommonErrorEventRaiser.RaiseErrorEvent(this, EventProfileNotFound, ce);
            }

            GettingProfile = false;
        }

        private async Task GetLatestSchedule()
        {
            try
            {
                GettingSchedule = true;
                await GetOfficeList();
                var currentSchedules = new ObservableCollection<Schedule>(await _scheduleService.GetSchedule());
                if (currentSchedules != null && currentSchedules.Count > 0)
                {
                    foreach (Schedule schedule in currentSchedules)
                    {
                        schedule.Login.PickupAddress = Profile.Address;
                        schedule.Login.DropAddress = GetOfficeAddress(schedule.Login.Venue); // Venue is always office address

                        schedule.Logout.PickupAddress = GetOfficeAddress(schedule.Logout.Venue); // Venue is always office address
                        schedule.Logout.DropAddress = Profile.Address;
                    }

                    if (Schedules != currentSchedules)
                    {
                        Schedules = currentSchedules;
                    }

                    ScheduleIsAvailable = true;

                }
                else
                {
                    ScheduleIsAvailable = false;
                }
                ScheduleNotFoundMessage = "no schedule found";

                var scheduleLoadEvent = new AnalyticEvent()
                {
                    EventName = FlurryConstants.Event.SchedulePanorma_LoadTime
                };
                double loadTime = (DateTime.Now - ScheduleStartTime).TotalMilliseconds;
                scheduleLoadEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.LoadTime_TimeSpanInMilliSeconds, loadTime.ToString()));

                LogAnalyticsEvent(scheduleLoadEvent);

            }

            catch (Exception ce)
            {
                ScheduleNotFoundMessage = "no schedule found";

                CommonErrorEventRaiser.RaiseErrorEvent(this, EventScheduleNotLoaded, ce);
                var noScheduleEvent = new AnalyticEvent()
                {
                    EventName = FlurryConstants.Event.NoScheduleFound
                };

                LogAnalyticsEvent(noScheduleEvent);
            }
            GettingSchedule = false;
        }

        private string GetOfficeAddress(string venueGuid)
        {

            if (OfficeVenues != null && OfficeVenues.Count > 0)
            {
                if (!string.IsNullOrWhiteSpace(venueGuid))
                {
                    if (OfficeVenues.Count(c => c.Guid == venueGuid) > 0)
                    {
                        return OfficeVenues.First(c => c.Guid == venueGuid).Address;
                    }
                }
            }

            return "Office";

        }

        private async Task GetTrackCabDetails()
        {
            try
            {
                await GetTripDetails();
                var TrackCabLoadEvent = new AnalyticEvent()
                {
                    EventName = FlurryConstants.Event.TrackCabPanorma_LoadTime
                };

                double loadTime = (DateTime.Now - TrackStartTime).TotalMilliseconds;
                TrackCabLoadEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.LoadTime_TimeSpanInMilliSeconds, loadTime.ToString()));
                LogAnalyticsEvent(TrackCabLoadEvent);
            }
            catch (Exception ce)
            {
                CommonErrorEventRaiser.RaiseErrorEvent(this, EventTrackCabNotFound, ce);
            }
        }

        #endregion

        #region EventHandlers

        public EventHandler EventScheduleNotLoaded;
        public EventHandler EventProfileNotFound;
        public EventHandler EventTrackCab;
        public EventHandler EventTrackCabNotFound;
        public EventHandler EventPlotMap;
        public EventHandler EventGotoTrackPage;


        private EmployeeProfile _profile;
        public EmployeeProfile Profile
        {
            get { return _profile; }
            set { _profile = value; RaisePropertyChanged(() => Profile); }
        }
        public Trip _tripDetail;
        public Trip TripDetails
        {
            get { return _tripDetail; }
            set { _tripDetail = value; RaisePropertyChanged(() => TripDetails); }
        }
        public TrackMilestone _trackCab;
        public TrackMilestone TrackCab
        {
            get { return _trackCab; }
            set { _trackCab = value; RaisePropertyChanged(() => TrackCab); }

        }

        #endregion


        public void ContenxtMenuEdit(ScheduleItem itemSchedule)
        {
            try
            {
                var parentScheule = itemSchedule.ParentSchedule;
                this.ShowComplexViewModel(typeof(EditScheduleViewModel), new Moveinsync.Core.ViewModels.EditScheduleViewModel.ScheduleInput()
                {
                    Schedule = parentScheule,
                    IsLogin = itemSchedule.Type.Equals("Login", StringComparison.CurrentCultureIgnoreCase)
                });

                AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                objAnalyticEvent.EventName = FlurryConstants.Event.EditSchedule_PageVisit;
                //call for EditSchedule_PageVisit

                LogAnalyticsEvent(objAnalyticEvent);

            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }

        }



        public async void CancelSchedule(ScheduleItem itemSchedule, string isLoginOrLogout)
        {
            try
            {

                bool isCancelled = string.Equals(itemSchedule.State, "Cancelled", StringComparison.CurrentCultureIgnoreCase);

                if (!isCancelled)
                {

                    string flag = await _scheduleService.CancelScheduleItem(itemSchedule.ParentSchedule, isLoginOrLogout);

                    if (flag == null || flag == "")
                    {
                        //call getschedule
                        ReloadScheduleTimer();
                    }
                    else
                    {

                    }
                }

            }
            catch (Exception ex)
            {
                ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
                //Close(this);
            }

        }

        public bool RaisingSOS { get; set; }

        public bool SOS_IsRaised { get; set; }

        private bool isLocationEnabled = false;

        public async void RaisePanicSOS(bool isLocationEnabled)
        {
            try
            {
                this.isLocationEnabled = isLocationEnabled;

                if (Settings.IsSOSEnabled && Settings.LongPressDuration.HasValue && Settings.IsTripDetailsEnabled && Settings.IsVehicleTrackingEnabled)
                {
                    RaisingSOS = true;

                    if (_watcher != null)
                    {
                        if (!_watcher.Started)
                            _watcher.Start(new MvxLocationOptions(), OnLocationFound, OnError);
                    }
                }
                else
                {
                    ThrowError(new ErrorEventArgs(AppResources.SOS_ERROR, null));
                }
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }

        }

        public string SOSRaiseServerMessage { get; set; }
        private async Task RiaseSOSAfterLocationCheck()
        {
            if (_watcher != null)
                _watcher.Stop();
            Panic panic = new Panic();

            try
            {
                bool ableToGetLocation = Lat != null && Lng != null;
                panic.time = (long)UnixTimeStamp.ConvertToUnixTimestamp(DateTime.UtcNow) * 1000;
                if (ableToGetLocation)
                {
                    panic.latitude = Lat.ToString();
                    panic.longitude = Lng.ToString();
                }
                else
                {
                    // We are sending default values 0, 0  for lat, lng if nothing is found
                    panic.latitude = "0";
                    panic.longitude = "0";
                }


                string SOSResponse = await _panicService.RaisePanic(panic);
                RaisingSOS = false;
                CallSOSSuccess(new PanicSuccessArgs() { Panic_Success_Message = SOSResponse });

                SOSRaiseServerMessage = SOSResponse;

                var sosEvent = new AnalyticEvent()
                {
                    EventName = FlurryConstants.Event.SOSRaised_Successful
                };
                sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.SOSClicked_LocationNotEnabled, isLocationEnabled.ToString()));
                sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.UnableToGetLocation, (!ableToGetLocation).ToString()));

                sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.LocationOfSOSLatitude, panic.latitude.ToString()));
                sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.LocationOfSOSLongitude, panic.longitude.ToString()));
                if (TripDetails != null && TripDetails.Cab != null && TripDetails.Pickup != null)
                {
                    sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.TripID, TripDetails.Cab.misVehicleId + "_" + TripDetails.Pickup.Time));
                    sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.Driver, TripDetails.Cab.driverName));
                    sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.VehicleId, TripDetails.Cab.misVehicleId));
                }
                else
                {
                    sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.TripID, "n/a"));
                    sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.Driver, "n/a"));
                    sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.VehicleId, "n/a"));
                }
                sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.UnixTimestampOfSOS, UnixTimeStamp.ConvertToUnixTimestamp(DateTime.Now).ToString()));
                sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.SMSComposed, "false"));

                LogAnalyticsEvent(sosEvent);

            }
            catch (Exception ce)
            {
                //Todo : Compose and SMS that user can send to raise SOS
                RaisingSOS = false;
                bool ableToGetLocation = Lat != null && Lng != null;

                double latitude = 0;
                double longitude = 0;

                if (ableToGetLocation)
                {
                    latitude = Lat.Value;
                    longitude = Lng.Value;
                    CallSOSThroughSMS(new PanicArgs() { latitude = latitude, longitude = longitude });
                }
                else
                {
                    CallSOSThroughSMS(new PanicArgs() { latitude = 0, longitude = 0 });
                }


                var sosEvent = new AnalyticEvent()
                {
                    EventName = FlurryConstants.Event.SOSRaised_Successful
                };
                sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.SOSClicked_LocationNotEnabled, isLocationEnabled.ToString()));
                sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.UnableToGetLocation, (!ableToGetLocation).ToString()));

                sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.LocationOfSOSLatitude, latitude.ToString()));
                sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.LocationOfSOSLongitude, longitude.ToString()));
                if (TripDetails != null && TripDetails.Cab != null && TripDetails.Pickup != null)
                {
                    sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.TripID, TripDetails.Cab.misVehicleId + "_" + TripDetails.Pickup.Time));
                    sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.Driver, TripDetails.Cab.driverName));
                    sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.VehicleId, TripDetails.Cab.misVehicleId));
                }
                else
                {
                    sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.TripID, "n/a"));
                    sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.Driver, "n/a"));
                    sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.VehicleId, "n/a"));
                }
                sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.UnixTimestampOfSOS, UnixTimeStamp.ConvertToUnixTimestamp(DateTime.Now).ToString()));
                sosEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.SMSComposed, "true"));

                LogAnalyticsEvent(sosEvent);
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ce));
            }
        }

        public EventHandler SendSOSThroughSMS;

        public void CallSOSThroughSMS(PanicArgs args)
        {
            try
            {
                if (SendSOSThroughSMS != null)
                    SendSOSThroughSMS(this, args);
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        public EventHandler SendSOSSuccess;

        public void CallSOSSuccess(PanicSuccessArgs args)
        {
            try
            {
                if (SendSOSSuccess != null)
                    SendSOSSuccess(this, args);
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private async void OnLocationFound(MvxGeoLocation location)
        {
            try
            {
                Lat = location.Coordinates.Latitude;
                Lng = location.Coordinates.Longitude;

                await RiaseSOSAfterLocationCheck();
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private async void OnError(MvxLocationError error)
        {
            try
            {
                Mvx.Error("Seen location error {0}", error.Code);

                if (_watcher.LastSeenLocation != null && (_watcher.LastSeenLocation.Timestamp.DateTime - DateTime.Now).Minutes <= 10)
                {
                    Lat = _watcher.LastSeenLocation.Coordinates.Latitude;
                    Lng = _watcher.LastSeenLocation.Coordinates.Longitude;
                }
                else
                {
                    Lat = null;
                    Lng = null;
                }

                await RiaseSOSAfterLocationCheck();
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }


        public EventHandler SettingsChangedEvent;
        public void SettingsChanged(EventArgs args)
        {
            if (SettingsChangedEvent != null)
                SettingsChangedEvent(this, args);
        }

        private double? _lng;
        public double? Lng
        {
            get { return _lng; }
            set { _lng = value; RaisePropertyChanged(() => Lng); }
        }

        private double? _lat;
        public double? Lat
        {
            get { return _lat; }
            set { _lat = value; RaisePropertyChanged(() => Lat); }
        }




        public void NavigateToAboutUs()
        {
            try
            {
                if (HasInternetConnection)
                {
                    ShowViewModel<AboutUsViewModel>();
                    AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                    objAnalyticEvent.EventName = FlurryConstants.Event.AboutUs_Visit;
                    LogAnalyticsEvent(objAnalyticEvent);
                }
            }
            catch (Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }
    }
}
