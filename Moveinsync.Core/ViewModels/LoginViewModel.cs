﻿using Cirrious.CrossCore;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.Plugins.File;
using Cirrious.MvvmCross.ViewModels;
using Moveinsync.Core.EventArguments;
using Moveinsync.Core.IServices;
using Moveinsync.Core.Model;
using Moveinsync.Core.Services;
using Moveinsync.Core.User;
using Moveinsync.Core.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Moveinsync.Core.Analytics;


namespace Moveinsync.Core.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IRegistrationService _registrationService;
        private readonly IValidateService _validationService;

        public LoginViewModel(IRegistrationService registrationService, IValidateService validationService, IMvxFileStore fileStore)
        {
            _registrationService = registrationService;
            _validationService = validationService;

            if (!string.IsNullOrWhiteSpace(UserSession.Profile.UserAuthorization))
            {
                this.ShowViewModel<HomeViewModel>();
            }
            IsNewNumberRegistration = true;

        }

        public string SessionInvalidErrorMessage { get; set; }
        public void Init(string errorMessage)
        {
            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                SessionInvalidErrorMessage = errorMessage;
            }
            else
            {
                SessionInvalidErrorMessage = null;
            }
        }


        private string _mobileNo;
        public string MobileNo
        {
            get { return _mobileNo; }

            set
            {

                _mobileNo = value;
                RaisePropertyChanged(() => MobileNo);
            }
        }

        private string _otp;
        public string OTP
        {
            get { return _otp; }
            set { _otp = value; RaisePropertyChanged(() => OTP); }
        }

        private string _message;
        public string Message
        {
            get { return _message; }
            set { _message = value; RaisePropertyChanged(() => Message); }
        }

        private string _commandMessage;
        public string CommandMessage
        {
            get { return _commandMessage; }
            set { _commandMessage = value; RaisePropertyChanged(() => CommandMessage); }
        }

        private Registration _reistration;
        public Registration Registration
        {
            get { return _reistration; }
            set { _reistration = value; RaisePropertyChanged(() => Registration); }
        }

        private IMvxCommand _dogoOTP;
        public IMvxCommand DoGoOTP
        {
            get
            {
                _dogoOTP = _dogoOTP ?? new MvxCommand(GetOTP);
                return _dogoOTP;
            }

        }


        private IMvxCommand _dogoMain;
        public IMvxCommand DoGoMain
        {
            get
            {
                _dogoMain = _dogoMain ?? new MvxCommand(ConfirmOTP);
                return _dogoMain;
            }

        }
        public bool IsNewNumberRegistration { get; set; }

        public EventHandler EvntRegistered;
        public void CallRegistered(RegisterEventArgs args)
        {
            if (EvntRegistered != null)
                EvntRegistered(this, args);
        }

        public EventHandler EvntValidatedOTP;
        public void ValidatedOTP(ValidateEventArgs args)
        {
            if (EvntValidatedOTP != null)
                EvntValidatedOTP(this, args);
        }


        private string _uuid;
        public string UUID
        {
            get { return _uuid; }
            set { _uuid = value; RaisePropertyChanged(() => UUID); }
        }

        public bool GettingDataFromService { get; set; }

        private bool sendOTPButtonIsEnabled = true;
        public bool SendOTPButtonIsEnabled
        {
            get { return sendOTPButtonIsEnabled; }
            set { sendOTPButtonIsEnabled = value;  }
        }


        public async void ConfirmOTP()
        {
           
            GettingDataFromService = true;
            try
            {

                if (ValidateOTP())
                {
                    bool validationResponse = await _validationService.GetValidated(OTP);
                    ValidateEventArgs objValidateEventArgs = new ValidateEventArgs();

                    //write code to store response string for future purpose

                    if (validationResponse == true)
                    //check response if ok than redirect to main panaroma 
                    {
                        objValidateEventArgs.IsValidated = true;
                        objValidateEventArgs.ErrorMessage = string.Empty;
                        ValidatedOTP(objValidateEventArgs);
                        UserSession.Profile = new UserProfile() { UserAuthorization = UserSession.TempUUID };
                        ShowViewModel<HomeViewModel>();
                    }
                    else
                    {
                        objValidateEventArgs.IsValidated = false;
                        objValidateEventArgs.ErrorMessage = "Invalid Token";
                        ValidatedOTP(objValidateEventArgs);
                    }
                }
                AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                objAnalyticEvent.EventName = FlurryConstants.Event.ConfirmOTP_Click;

                objAnalyticEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.EmployeeMobile, MobileNo.ToString()));
                objAnalyticEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.OTP, MobileNo.ToString()));
                objAnalyticEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.UUID, UserSession.TempUUID));
                LogAnalyticsEvent(objAnalyticEvent);


            }
            catch (Exception ce)
            {
                ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ce));
            }
            GettingDataFromService = false;
           
        }


        public async void GetOTP()
        {
            SendOTPButtonIsEnabled = false;
            GettingDataFromService = true;
            if (ValidateRegistration())
            {
                try
                {
                    AnalyticEvent objAnalyticEvent = new AnalyticEvent();

                    Registration registrationResponse = await _registrationService.GetRegistered(MobileNo);

                    if (registrationResponse.IsRegistered)
                    {
                        IsNewNumberRegistration = false;
                        objAnalyticEvent.EventName = FlurryConstants.Event.ResendOTP_Click;

                        objAnalyticEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.EmployeeMobile, MobileNo.ToString()));
                        objAnalyticEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.UUID, UserSession.TempUUID));

                        // send emp mobile no
                        LogAnalyticsEvent(objAnalyticEvent);

                    }
                    else
                    {
                        IsNewNumberRegistration = true;
                    }

                    CallRegistered(new RegisterEventArgs() { IsRegisterd = registrationResponse.IsRegistered, ErrorMessage = CustomErrorMessages.GetcustomErrorMessages(Convert.ToInt32(registrationResponse.ErrorCode)) });


                    objAnalyticEvent.EventName = FlurryConstants.Event.EmployeeMobile;

                    objAnalyticEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.EmployeeMobile, MobileNo.ToString()));
                    objAnalyticEvent.Parameters.Add(new KeyValuePair<string, string>(FlurryConstants.EventParameter.UUID, UserSession.TempUUID));

                    // send emp mobile no
                    LogAnalyticsEvent(objAnalyticEvent);
                    // get otp click event
                    objAnalyticEvent.EventName = FlurryConstants.Event.GETOTP_Click;
                    LogAnalyticsEvent(objAnalyticEvent);


                }
                catch (Exception ce)
                {
                    ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ce));                   
                    
                }
            }
            GettingDataFromService = false;
            SendOTPButtonIsEnabled = true;
        }


        private bool ValidateOTP()
        {
           
                List<string> errors = new List<string>();
                //Check if the textbox has vlaid tenb digit number etc.
                long Tempotpvalue = 0;
                if (!(long.TryParse(OTP, out Tempotpvalue) && OTP.Length == 6))
                {
                    errors.Add("Please enter a valid 6 digit OTP");
                }

                if (errors.Count > 0)
                {
                    ThrowError(new ErrorEventArgs(errors));
                    return false;
                }
        
            return true;

        }

        private bool ValidateRegistration()
        {

            List<string> errors = new List<string>();
            //Check if the textbox has vlaid tenb digit number etc.
            long mobileNoValue = 0;
            if (!(long.TryParse(MobileNo, out mobileNoValue) && mobileNoValue > 999999999 && mobileNoValue < 10000000000))
            {
                errors.Add("Please enter a valid 10 digit number");
            }
            if (errors.Count > 0)
            {
                ThrowError(new ErrorEventArgs(errors));
                return false;
            }

            return true;

        }


        public void GoToTermsAndConditions()
        {
            try
            {
                if (HasInternetConnection)
                {
                    ShowViewModel<TermsAndConditionsViewModel>();
                }
            }
            catch(Exception ex)
            {
                LogError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }
    }
}
