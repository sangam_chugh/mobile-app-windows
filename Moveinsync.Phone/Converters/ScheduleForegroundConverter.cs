﻿using Moveinsync.Core.Model;
using Moveinsync.Core.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Moveinsync.Phone.Converters
{
    public class ScheduleForegroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string foregroundColor = "Black";
            if (value is ScheduleItem)
            {
                var item = value as ScheduleItem;
                if (item.Hours >= 0)
                {
                    if (string.Equals(item.State, "Scheduled", StringComparison.CurrentCultureIgnoreCase))
                    {
                        foregroundColor = "Black";                       
                    }
                    else
                    {
                        foregroundColor = "Red";                       
                    }

                }
                else if (item.Hours == -1)
                {
                    if (string.IsNullOrEmpty(item.State))
                    {
                        foregroundColor = "Orange";
                        
                    }
                    else
                    {
                        foregroundColor = "Red";                        
                    }

                }
                else if (item.Hours == -2)
                {
                    foregroundColor = "Gray";
                    
                }
            }

            return foregroundColor;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
