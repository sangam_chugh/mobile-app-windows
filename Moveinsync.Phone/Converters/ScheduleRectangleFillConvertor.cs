﻿using Moveinsync.Core.Model;
using Moveinsync.Core.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Moveinsync.Phone.Converters
{
    public class ScheduleRectangleFillConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string fillColor = "#4A7A06";
            if (value is ScheduleItem)
            {
                var item = value as ScheduleItem;
                if (item.Type == "LOGIN")
                {
                    fillColor = "#4A7A06";
                }
                else
                {
                    fillColor = "#3AAEE1";
                }
            }
            return fillColor;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
