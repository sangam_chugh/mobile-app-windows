﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Moveinsync.Phone.Converters
{
    public class TrackingAvailabilityMsg : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string TrackingUnAvailable = "cab location unavailable";  //Previous : "No cabs assigned"
            if (value == null)
                return TrackingUnAvailable;
            if (value is bool)
            {
                var item = (bool)value;
                if (item == false)
                {
                    return TrackingUnAvailable;

                }
                else
                    return "";
            }
            return TrackingUnAvailable;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
