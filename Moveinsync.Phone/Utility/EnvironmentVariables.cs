﻿using Microsoft.Phone.Info;
using Microsoft.Phone.Net.NetworkInformation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;


namespace Moveinsync.Phone.Utility
{
    public class EnvironmentVariables
    {
        private static string appVersion;
        public static string AppVersion
        {
            get
            {
                if (appVersion == null)
                {
                    try
                    {
                        var xmlReaderSettings = new XmlReaderSettings
                        {
                            XmlResolver = new XmlXapResolver()
                        };

                        using (var xmlReader = XmlReader.Create("WMAppManifest.xml", xmlReaderSettings))
                        {
                            xmlReader.ReadToDescendant("App");
                            appVersion = xmlReader.GetAttribute("Version");
                            return appVersion;
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
                return appVersion;
            }

        }




        private static string osVersion;
        public static string OSVersion
        {
            get
            {
                if (osVersion == null)
                {
                    try
                    {
                        osVersion = System.Environment.OSVersion.ToString();
                        return (osVersion);
                    }
                    catch (Exception ex)
                    {
                    }
                }
                return osVersion;
            }

        }

        private static string screenSize;
        public static string ScreenSize
        {
            get
            {
                if (screenSize == null)
                {
                    try
                    {
                        string ScreenWidth = Application.Current.Host.Content.ActualWidth.ToString();
                        string ScreenHeight = Application.Current.Host.Content.ActualHeight.ToString();
                        screenSize = "Width =" + ScreenWidth + "Height = " + ScreenHeight;
                        return screenSize;
                    }
                    catch (Exception ex)
                    {
                    }
                }
                return screenSize;
            }

        }



        private static string deviceMake;
        public static string DeviceMake
        {
            get
            {
                if (deviceMake == null)
                {
                    try
                    {
                        deviceMake = DeviceExtendedProperties.GetValue("DeviceName").ToString();
                        return DeviceMake;
                    }
                    catch (Exception ex)
                    {
                    }
                }
                return deviceMake;
            }

        }


        private static string manufacturer;
        public static string Manufacturer
        {
            get
            {
                if (manufacturer == null)
                {
                    try
                    {
                        manufacturer = DeviceExtendedProperties.GetValue("DeviceManufacturer").ToString();
                        return manufacturer;
                    }
                    catch (Exception ex)
                    {
                    }
                }
                return manufacturer;
            }

        }

        private static string deviceUUDI;
        public static string DeviceUUDI
        {
            get
            {
                if (deviceUUDI == null)
                {
                    try
                    {
                        deviceUUDI = Convert.ToBase64String(((byte[])DeviceExtendedProperties.GetValue("DeviceUniqueId")));
                       return deviceUUDI;
                    }
                    catch (Exception ex)
                    {
                    }
                }
                return manufacturer;
            }

        }

     
    }
}
