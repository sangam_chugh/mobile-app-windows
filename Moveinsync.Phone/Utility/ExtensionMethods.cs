﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace Moveinsync.Phone.Utility
{
    public static class ExtensionMethods
    {
        public static void UpdateBinding(this TextBox textBox)
        {
            BindingExpression bindingExpression =
                    textBox.GetBindingExpression(TextBox.TextProperty);
            if (bindingExpression != null)
            {
                bindingExpression.UpdateSource();
            }
        }
    }
}
