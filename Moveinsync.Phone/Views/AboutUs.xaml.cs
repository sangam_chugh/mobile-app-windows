﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Moveinsync.Core.ViewModels;
using Moveinsync.Core.Utility;

namespace Moveinsync.Phone.Views
{
    public partial class AboutUs : BasePhonePage
    {
        public AboutUs()
        {
            InitializeComponent();
        }

        public new AboutUsViewModel ViewModel
        {
            get { return (AboutUsViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }
       
    }
}