﻿using Cirrious.MvvmCross.WindowsPhone.Views;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Net.NetworkInformation;
using Moveinsync.Core.Analytics;
using Moveinsync.Core.EventArguments;
using Moveinsync.Core.ViewModels;
using Moveinsync.Phone.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;


namespace Moveinsync.Phone.Views
{
    public class BasePhonePage : MvxPhonePage
    {
        public BasePhonePage()
        {
           this.BackKeyPress += BasePhonePage_BackKeyPress;
        }

        public new BaseViewModel ViewModel
        {
            get { return (BaseViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }
        
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ViewModel.OnResume();

            if (ViewModel.EvntThrowError == null)
            {
                ViewModel.EvntThrowError += new EventHandler((object sender, EventArgs ea) =>
                {
                    ShowError(((ErrorEventArgs)ea));
                });           
            }

            if (ViewModel.EvntLogError == null)
            {
                ViewModel.EvntLogError += new EventHandler((object sender, EventArgs ea) =>
                {
                    LogError(((ErrorEventArgs)ea));
                });
            }

            if (ViewModel.EvntLogAnalytics == null)
            {
                ViewModel.EvntLogAnalytics += new EventHandler((object sender, EventArgs ea) =>
                {
                    if (ea is AnalyticEventArgs && ((AnalyticEventArgs)ea).EventLog != null)
                    {
                        LogAnalyticEvent(((AnalyticEventArgs)ea).EventLog);
                    }
                    
                });
            }
         
        }

        protected void LogAnalyticEvent(Moveinsync.Core.Analytics.AnalyticEvent analyticEvent)
        {
            if (analyticEvent.Parameters.Count > 0)
            {
                FlurryWP8SDK.Api.LogEvent(analyticEvent.EventName,
                analyticEvent.Parameters.Select(c => new FlurryWP8SDK.Models.Parameter(c.Key, c.Value)).ToList());
            }
            else
            {
                FlurryWP8SDK.Api.LogEvent(analyticEvent.EventName);
            }
            
        }

        private void LogError(ErrorEventArgs args)
        {
            if (args.Exception != null)
            {
                //CrittercismSDK.Crittercism.LogHandledException(args.Exception);
            }
            
        }

        private void ShowError(ErrorEventArgs errorEventArgs)
        { 
            ShowError(errorEventArgs.Errors);
            AnalyticEvent objAnalyticEvent = new AnalyticEvent();
            objAnalyticEvent.EventName = FlurryConstants.Event.Alerts;

            objAnalyticEvent.Parameters.Add(new KeyValuePair<string, string>("Alert", errorEventArgs.Errors.SingleOrDefault().ToString()));
            //call for ValidatePanorama_PageVisit
            LogAnalyticEvent(objAnalyticEvent);
        }

        protected void ShowError(List<string> errors)
        {
            MessageBox.Show(errors.SingleOrDefault());
        }

        protected void ShowError(string error)
        {
            var errors = new List<string>();
            errors.Add(error);
            ShowError(errors);
        }      

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            ViewModel.OnPause();
        }

        protected void BasePhonePage_BackKeyPress(object sender, CancelEventArgs e)
        {
            if (!NavigationService.CanGoBack)
            {
                if (MessageBox.Show("Are you sure you want to exit?", "Exit?",
                                  MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                {
                    e.Cancel = true;                   
                }
                else
                {
                    AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                    objAnalyticEvent.EventName = FlurryConstants.Event.ExitEvent;                   
                    LogAnalyticEvent(objAnalyticEvent);
                }

                //Use below code if you want to turn ok/cancel box position

                //e.Cancel = true;
                //Deployment.Current.Dispatcher.BeginInvoke(() =>
                //{
                //    CustomMessageBox messageBox = new CustomMessageBox()
                //    {
                //        Caption = "Exit",
                //        Message = AppResources.ON_BACK_BUTTON_PRESS_EXIT_APP,
                //        LeftButtonContent = "Cancel",
                //        RightButtonContent = "OK"
                //    };

                //    messageBox.Dismissed += (s1, e1) =>
                //    {
                //        switch (e1.Result)
                //        {
                //            case CustomMessageBoxResult.LeftButton:
                //                // Do Nothing.
                //                break;
                //            case CustomMessageBoxResult.RightButton:
                //                if (!NavigationService.CanGoBack)
                //                {
                //                    AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                //                    objAnalyticEvent.EventName = FlurryConstants.Event.ExitEvent;
                //                    //call edittrip
                //                    LogAnalyticEvent(objAnalyticEvent);

                //                    System.IO.IsolatedStorage.IsolatedStorageSettings.ApplicationSettings.Save();
                //                    Application.Current.Terminate();
                //                }
                //                else
                //                {
                //                    NavigationService.GoBack();
                //                }
                //                break;
                //            default:
                //                break;
                //        }
                //    };

                //    messageBox.Show();
                //});
            }

        }

    }
}
