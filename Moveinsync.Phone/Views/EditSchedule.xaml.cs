﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Moveinsync.Core.ViewModels;
using Microsoft.Phone.Net.NetworkInformation;
using Moveinsync.Core.EventArguments;
using Moveinsync.Phone.Resources;
using Moveinsync.Core.Utility;

namespace Moveinsync.Phone.Views
{
    public partial class EditSchedule : BasePhonePage
    {
        public EditSchedule()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (ViewModel.ScheduledPostEvent == null)
            {
                ViewModel.ScheduledPostEvent += new EventHandler((object sender, EventArgs ea) =>
                {
                    ScheduledPostEvent_Handler((ScheduleArgs)ea);
                });
            }
            ViewModel.ShiftWarning += new EventHandler((object sender, EventArgs ea) =>
            {
                ShiftWarning_Handler((EventArgs)ea);
            });
            
            //if (ViewModel.ShiftWarning==false)
            //{ MessageBox.Show("It seems your logout will be past midnight. if so, please schedule your logout for next day"); }

        }

        private void ShiftWarning_Handler(EventArgs scheduleArgs)
        {
           // throw new NotImplementedException();
            MessageBox.Show("It seems your logout will be past midnight. if so, please schedule your logout for next day");
        }
        private void ScheduledPostEvent_Handler(ScheduleArgs args)
        {
            try
            {
                if (args.Message == "success")
                    MessageBox.Show("Schedule updated successfully");
                else if (args.Message == "success4pm")
                    MessageBox.Show("Schedule updated successfully. It seems your logout will be past midnight. if so, please schedule your logout for next day");
                else
                {
                    MessageBox.Show("Schedule not updated successfully");
                }
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        public new EditScheduleViewModel ViewModel
        {
            get { return (EditScheduleViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                ViewModel.GoBackToSchedule();
            }

            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                CustomMessageBox messageBox = new CustomMessageBox()
                {
                    Caption = "Edit Trip",
                    Message = AppResources.EDIT_SCHEDUELE_USER_COFIRM_MESSAGE,
                    LeftButtonContent = "Cancel",
                    RightButtonContent = "OK"
                };

                messageBox.Dismissed += (s1, e1) =>
                {
                    switch (e1.Result)
                    {
                        case CustomMessageBoxResult.LeftButton:
                            // Do something.
                            break;
                        case CustomMessageBoxResult.RightButton:
                            ViewModel.SaveNewShift();
                            break;
                        default:
                            break;
                    }
                };

                messageBox.Show();
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }
    }
}