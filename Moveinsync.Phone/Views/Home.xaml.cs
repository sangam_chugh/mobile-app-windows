﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Cirrious.MvvmCross.WindowsPhone.Views;
using Cirrious.MvvmCross.ViewModels;
using Moveinsync.Core.Model;
using Moveinsync.Core.ViewModels;
using Moveinsync.Core.Utility;
using Moveinsync.Core.EventArguments;
using Moveinsync.Core.Services;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Device.Location;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Maps.Controls;
using Moveinsync.Phone.Resources;
using Microsoft.Phone.Tasks;
using System.Windows.Threading;
using Windows.System;
using TimerReference.Core;
using System.ComponentModel;
using Moveinsync.Core.Analytics;
using Microsoft.Devices;
using Microsoft.Phone.Maps.Services;



namespace Moveinsync.Phone.Views
{//PostRegistration
    public partial class Home : BasePhonePage
    {
        public bool LocationEnabled { get; set; }
        public Home()
        {

            InitializeComponent();
            if (DashboardPanorama.Items.Count > 1)
            {
                DashboardPanorama.DefaultItem = DashboardPanorama.Items[1];
            }

        }



        private void locator_StatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            if (args.Status != PositionStatus.Disabled)
                LocationEnabled = true;
            else
                LocationEnabled = false;

            Deployment.Current.Dispatcher.BeginInvoke(() =>
           {
               if (LocationEnabled)
               {

                   LocationSetingsWarning.Visibility = Visibility.Collapsed;
               }
               else
               {
                   LocationSetingsWarning.Visibility = Visibility.Visible;
               }
           });

        }

        public new HomeViewModel ViewModel
        {
            get { return (HomeViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            try
            {
                base.OnNavigatedTo(e);
                MapObj.Center = new GeoCoordinate(12.9100, 77.6400);
                while (NavigationService.CanGoBack)
                {
                    NavigationService.RemoveBackEntry();
                }


                Geolocator locator = new Geolocator();
                locator.DesiredAccuracy = PositionAccuracy.High;
                locator.MovementThreshold = 10;

                if (locator.LocationStatus == PositionStatus.Disabled)
                {
                    LocationEnabled = false;
                }
                else
                {
                    LocationEnabled = false;
                }

                locator.StatusChanged += locator_StatusChanged;
                CenterMapToUserLocationIfAvailable(locator);


                if (LocationEnabled)
                {
                    LocationSetingsWarning.Visibility = Visibility.Collapsed;
                }
                else
                {
                    LocationSetingsWarning.Visibility = Visibility.Visible;
                }

                if (ViewModel.EventScheduleNotLoaded == null)
                {
                    ViewModel.EventScheduleNotLoaded += new EventHandler((object sender, EventArgs ea) =>
                    {
                        ScheduleNotLoaded((ErrorEventArgs)ea);
                    });
                }
                if (ViewModel.EventProfileNotFound == null)
                {
                    ViewModel.EventProfileNotFound += new EventHandler((object sender, EventArgs ea) =>
                    {
                        ProfileNotFound_Handler((ErrorEventArgs)ea);
                    });
                }
                if (ViewModel.EventTrackCabNotFound == null)
                {
                    ViewModel.EventTrackCabNotFound += new EventHandler((object sender, EventArgs ea) =>
                    {
                        EventTrackCabError_Handler((ErrorEventArgs)ea);
                    });
                }
                if (ViewModel.EventTrackCab == null)
                {
                    ViewModel.EventTrackCab += new EventHandler((object sender, EventArgs ea) =>
                    {
                        EventTrackCab_Handler((TrackCabEventArgs)ea);
                    });
                }

                if (ViewModel.EventPlotMap == null)
                {
                    ViewModel.EventPlotMap += new EventHandler((object sender, EventArgs ea) =>
                    {
                        EventPlotMap_Handler((MapArguments)ea);
                    });
                }

                if (ViewModel.SendSOSThroughSMS == null)
                {
                    ViewModel.SendSOSThroughSMS += new EventHandler((object sender, EventArgs ea) =>
                      {
                          SendSOSThroughSMS_Handler((PanicArgs)ea);
                      });
                }

                if (ViewModel.SendSOSSuccess == null)
                {
                    ViewModel.SendSOSSuccess += new EventHandler((object sender, EventArgs ea) =>
                    {
                        SendSOSSuccess_Handler((PanicSuccessArgs)ea);
                    });
                }

                if (ViewModel.SettingsChangedEvent == null)
                {
                    ViewModel.SettingsChangedEvent += new EventHandler((object sender, EventArgs ea) =>
                    {
                        SettingsChangedEvent();
                    });
                }


                if (ViewModel.EventGotoTrackPage == null)
                {
                    ViewModel.EventGotoTrackPage += new EventHandler((object sender, EventArgs ea) =>
                    {
                        EventGotoTrackPage_Handler(ea);
                    });
                }

            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void SettingsChangedEvent()
        {
            try
            {
                if (ViewModel.Settings != null)
                {
                    if ((ViewModel.Settings.IsProfileEnabled) == false)
                    {
                        (DashboardPanorama.Items[0] as PanoramaItem).Visibility = System.Windows.Visibility.Collapsed;
                        (ApplicationBar.Buttons[2] as ApplicationBarIconButton).IsEnabled = false;
                    }
                    else
                    {
                        (DashboardPanorama.Items[0] as PanoramaItem).Visibility = System.Windows.Visibility.Visible;
                        (ApplicationBar.Buttons[2] as ApplicationBarIconButton).IsEnabled = true;
                    }

                    if ((ViewModel.Settings.IsScheduleEnabled) == false)
                    {
                        (DashboardPanorama.Items[1] as PanoramaItem).Visibility = System.Windows.Visibility.Collapsed;
                        (ApplicationBar.Buttons[1] as ApplicationBarIconButton).IsEnabled = false;
                    }
                    else
                    {
                        (DashboardPanorama.Items[1] as PanoramaItem).Visibility = System.Windows.Visibility.Visible;
                        (ApplicationBar.Buttons[1] as ApplicationBarIconButton).IsEnabled = true;
                    }


                    if (!ViewModel.Settings.IsTripDetailsEnabled && !ViewModel.Settings.IsVehicleTrackingEnabled && !ViewModel.Settings.IsSOSEnabled)
                    {
                        (DashboardPanorama.Items[2] as PanoramaItem).Visibility = System.Windows.Visibility.Collapsed;
                        (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = false;
                    }
                    else
                    {
                        (DashboardPanorama.Items[2] as PanoramaItem).Visibility = System.Windows.Visibility.Visible;
                        (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = true;
                    }

                }
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }

        }

        private async void CenterMapToUserLocationIfAvailable(Geolocator locator)
        {
            try
            {
                if (LocationEnabled)
                {
                    var location = await locator.GetGeopositionAsync(new TimeSpan(0, 30, 0), new TimeSpan(0, 0, 10));
                    if (location != null && !mapCenterChanged)
                    {
                        MapObj.Center = location.Coordinate.ToGeoCoordinate();
                    }
                }
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void EventGotoTrackPage_Handler(EventArgs panicSuccessArgs)
        {
            try
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    GoToTrackPanoramaItem();
                });
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void SendSOSSuccess_Handler(PanicSuccessArgs panicSuccessArgs)
        {
            try
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    VibrateController vibrate = VibrateController.Default;
                    vibrate.Start(TimeSpan.FromMilliseconds(3000));
                    MessageBox.Show(panicSuccessArgs.Panic_Success_Message);
                    SOSRaisedPanel.Visibility = System.Windows.Visibility.Visible;
                });
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void SendSOSThroughSMS_Handler(PanicArgs panicArgs)
        {
            try
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
               {
                   VibrateController vibrate = VibrateController.Default;
                   vibrate.Start(TimeSpan.FromMilliseconds(3000));

                   SmsComposeTask smsComposeTask = new SmsComposeTask();
                   smsComposeTask.To = "9220092200";
                   smsComposeTask.Body = "MVSC SOS raised at Location " + panicArgs.latitude + "," + panicArgs.longitude;
                   smsComposeTask.Show();
                   MessageBox.Show(AppResources.PANIC_SERVICE_SUCCESS_MESSAGE);

               });
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        bool mapCenterChanged = false;

        GeoCoordinate[] geoArr = null;
        RouteQuery myQuery = null;

        TrackMilestone lastMilestone = null;
        TrackMilestone currentMilestone = null;
        string lastSeenTime = string.Empty;
        private async void EventPlotMap_Handler(MapArguments args)
        {

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                   
                    
                    if (!string.IsNullOrEmpty(args.LastSeentime))
                    {
                        lastSeenTime = args.LastSeentime;
                        if (ViewModel.currentTripMilestones.Count > 1)
                        {
                            List<GeoCoordinate> myCoordinates = new List<GeoCoordinate>();

                            if (currentMilestone != ViewModel.currentTripMilestones[ViewModel.currentTripMilestones.Count - 1])
                            {

                                lastMilestone = ViewModel.currentTripMilestones[ViewModel.currentTripMilestones.Count - 2];
                                currentMilestone = ViewModel.currentTripMilestones[ViewModel.currentTripMilestones.Count - 1];

                                myCoordinates.Add(new GeoCoordinate(lastMilestone.Latitude, lastMilestone.Longitude, 0, 0, 0, lastMilestone.Speed, lastMilestone.Bearing));
                                myCoordinates.Add(new GeoCoordinate(currentMilestone.Latitude, currentMilestone.Longitude, 0, 0, 0, currentMilestone.Speed, currentMilestone.Bearing));

                                myQuery = new RouteQuery();
                                myQuery.Waypoints = myCoordinates;
                                myQuery.QueryCompleted += MyQuery_QueryCompleted;
                                myQuery.QueryAsync();
                            }
                        }
                        else
                        {
                            DrawMapPins(new GeoCoordinate(args.LatestLatitude, args.LatestLongitude));
                        }
                    }
                    else
                    {
                        geoArr = null;
                        KillMapPointsTimer();
                        lock (routeQueue)
                        {
                            routeQueue = new Queue<GeoCoordinate>();
                        }
                        MapObj.Layers.Clear();
                        
                    }
                }
                catch (Exception ex)
                {
                    ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
                }
            });
        }
       
        Queue<GeoCoordinate> routeQueue = new Queue<GeoCoordinate>();
        PCLTimer mapPointsTimer;
        void MyQuery_QueryCompleted(object sender, QueryCompletedEventArgs<Route> e)
        {
            if (e.Error == null)
            {
                Route myRoute = e.Result;

                if (myRoute.Geometry.Count > 0)
                {
                    lock (routeQueue)
                    {
                        routeQueue = new Queue<GeoCoordinate>(myRoute.Geometry.ToList());
                        ViewModel.RouteListPointCount = routeQueue.Count;
                        ReloadMapPointsTimer();
                    }
                }
               
            }
        }

        private void ReloadMapPointsTimer()
        {

            if (mapPointsTimer == null)
            {
                //set up timer to run every second
                mapPointsTimer = new PCLTimer(new Action(MapPoints), TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(ViewModel.Settings.VehicleTracking.MapMilestoneDrawTime));
            }
            //timer starts one second from now
            mapPointsTimer.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(ViewModel.Settings.VehicleTracking.MapMilestoneDrawTime));
        }

        private void MapPoints()
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                lock (routeQueue)
                {
                    if (routeQueue.Count > 0)
                    {
                        GeoCoordinate cabCoordinate = routeQueue.Dequeue();
                        DrawMapPins(cabCoordinate);
                    }
                    else
                    {
                        KillMapPointsTimer();
                    }
                }                
            });
            
        }

        private void KillMapPointsTimer()
        {
            if (mapPointsTimer != null)
            {
                mapPointsTimer.Dispose();
                mapPointsTimer = null;
            }
        }

        private void DrawMapPins(GeoCoordinate cabCoordinate)
        {
            MapObj.Layers.Clear();
            
            geoArr = new GeoCoordinate[3];
            geoArr[0] = new GeoCoordinate(cabCoordinate.Latitude, cabCoordinate.Longitude);
            geoArr[1] = new GeoCoordinate(ViewModel.TripDetails.Drop.Latitude, ViewModel.TripDetails.Drop.Longitude);
            geoArr[2] = new GeoCoordinate(ViewModel.TripDetails.Pickup.Latitude, ViewModel.TripDetails.Pickup.Longitude);


            LocationRectangle setRect = null;
            setRect = LocationRectangle.CreateBoundingRectangle(geoArr);
            if (!ViewModel.HasTripBeenPlotted)
                MapObj.SetView(setRect);
            mapCenterChanged = true;

            AddCabPin(cabCoordinate.Latitude, cabCoordinate.Longitude, lastSeenTime);
            AddPickupPin();
            AddDropPin();
        }

        private void AddDropPin()
        {
            try
            {
                Grid markerGrid = new Grid();
                markerGrid.RowDefinitions.Add(new RowDefinition());
                markerGrid.RowDefinitions.Add(new RowDefinition());
                markerGrid.Background = new SolidColorBrush(Colors.Transparent);


                Image marker = new Image();
                marker.Source = new BitmapImage(new Uri("/Assets/Images/drop.png", UriKind.Relative));
                markerGrid.Children.Add(marker);

                MapOverlay markerOverlay = new MapOverlay();
                markerOverlay.Content = markerGrid;
                markerOverlay.GeoCoordinate = new GeoCoordinate(ViewModel.TripDetails.Drop.Latitude, ViewModel.TripDetails.Drop.Longitude);
                markerOverlay.PositionOrigin = new Point(0, 0.5);

                MapLayer mapLayer = new MapLayer();
                mapLayer.Add(markerOverlay);
                MapObj.Layers.Add(mapLayer);
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void AddPickupPin()
        {
            try
            {
                Grid markerGrid = new Grid();
                markerGrid.RowDefinitions.Add(new RowDefinition());
                markerGrid.RowDefinitions.Add(new RowDefinition());
                markerGrid.Background = new SolidColorBrush(Colors.Transparent);


                Image marker = new Image();
                marker.Source = new BitmapImage(new Uri("/Assets/Images/pickup.png", UriKind.Relative));
                markerGrid.Children.Add(marker);

                MapOverlay markerOverlay = new MapOverlay();
                markerOverlay.Content = markerGrid;
                markerOverlay.GeoCoordinate = new GeoCoordinate(ViewModel.TripDetails.Pickup.Latitude, ViewModel.TripDetails.Pickup.Longitude);
                markerOverlay.PositionOrigin = new Point(0, 0.5);

                MapLayer mapLayer = new MapLayer();
                mapLayer.Add(markerOverlay);
                MapObj.Layers.Add(mapLayer);
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void AddCabPin(double latitude, double longitude, string lastSeenTime)
        {
            try
            {
                Grid markerGrid = new Grid();
                markerGrid.RowDefinitions.Add(new RowDefinition());
                markerGrid.RowDefinitions.Add(new RowDefinition());
                markerGrid.Background = new SolidColorBrush(Colors.Transparent);

                /*
                <Panel Margin="-140,0,0,0">
                    <Border CornerRadius="20" Background="Black">
                        <StackPanel name="messagePanel" margin="10, 0, 10, 0" Orientation="Horizontal">
                            <StackPanel name="driverPanel" margin="10, 0, 0, 0" Width ="140" Canvas.Z-Index="3">
                                <Textblock name="driverRegNumber"/>
                                <Textblock name="driverName" Width="140"/>
                            </StackPanel>
                            <StackPanel name="lastSeenPanel" margin="10, 0, 10, 0" Width ="100" >
                                <Textblock name="lastSeenMessage"/>
                                <Textblock name="LastSeenTime"/>
                            </StackPanel>
                        </StackPanel>
                    </Border>
                    <Image name="marker" Margin ="0,-45,0,0" Canvas.Z-Index="6"/>
                </Panel>

             
                */
                StackPanel panel = new StackPanel();
                panel.Margin = new Thickness(-200, 0, 0, 0);

                Border boder = new Border();
                boder.CornerRadius = new CornerRadius(20);
                boder.Background = new SolidColorBrush(Colors.Black);

                StackPanel messagePanel = new StackPanel();
                messagePanel.Margin = new Thickness(10, 0, 10, 0);
                messagePanel.Orientation = System.Windows.Controls.Orientation.Horizontal;

                StackPanel driverPanel = new StackPanel();
                driverPanel.Width = 160;
                driverPanel.Margin = new Thickness(10, 0, 0, 0);

                TextBlock driverRegNumber = new TextBlock();
                driverRegNumber.FontSize = 16;
                driverRegNumber.Text = ViewModel.TripDetails.Cab.vehicleRegNumber;
                driverRegNumber.Foreground = new SolidColorBrush(Color.FromArgb(255, 145, 194, 54)); //#91c236     

                TextBlock driverName = new TextBlock();
                driverName.FontSize = 16;
                driverName.MaxWidth = driverName.MinWidth = driverName.Width = 140;
                driverName.Text = ViewModel.TripDetails.Cab.driverName;
                driverName.TextWrapping = TextWrapping.NoWrap;
                driverName.TextTrimming = TextTrimming.WordEllipsis;
                driverName.Foreground = new SolidColorBrush(Colors.White);

                driverPanel.Children.Add(driverRegNumber);
                driverPanel.Children.Add(driverName);

                messagePanel.Children.Add(driverPanel);


                StackPanel lastSeenPanel = new StackPanel();
                lastSeenPanel.Width = 85;
                lastSeenPanel.Margin = new Thickness(0, 0, 10, 0);

                TextBlock lastSeenMessage = new TextBlock();
                lastSeenMessage.FontSize = 16;
                lastSeenMessage.Text = "Last Seen";
                lastSeenMessage.Foreground = new SolidColorBrush(Color.FromArgb(255, 145, 194, 54)); //#91c236

                TextBlock LastSeenTime = new TextBlock();
                LastSeenTime.FontSize = 16;
                LastSeenTime.Text = lastSeenTime;
                LastSeenTime.Foreground = new SolidColorBrush(Colors.White);

                lastSeenPanel.Children.Add(lastSeenMessage);
                lastSeenPanel.Children.Add(LastSeenTime);

                messagePanel.Children.Add(lastSeenPanel);

                boder.Child = messagePanel;

                panel.Children.Add(boder);
                Image marker = new Image();
                marker.Source = new BitmapImage(new Uri("/Assets/Images/Cabicon.png", UriKind.Relative));
                marker.Margin = new Thickness(30, -45, 0, 0);
                Canvas.SetZIndex(driverPanel, 3);
                Canvas.SetZIndex(marker, 6);
                panel.Children.Add(marker);


                markerGrid.Children.Add(panel);

                MapOverlay markerOverlay = new MapOverlay();
                markerOverlay.Content = markerGrid;
                markerOverlay.GeoCoordinate = new GeoCoordinate(latitude, longitude);
                markerOverlay.PositionOrigin = new Point(0, 0.5);

                MapLayer mapLayer = new MapLayer();
                mapLayer.Add(markerOverlay);
                MapObj.Layers.Add(mapLayer);
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }


        private void EventTrackCabError_Handler(ErrorEventArgs args)
        {
            try
            {
                MessageBox.Show(args.Errors[0].ToString());
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void EventTrackCab_Handler(TrackCabEventArgs args)
        {
            try
            {
                if (args.IsTrackDetailsAvailable)
                {

                    MessageBox.Show("TrackCab loaded successfully");

                }
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        public void ScheduleNotLoaded(ErrorEventArgs args)
        {
            try
            {
                MessageBox.Show(args.Errors.SingleOrDefault());
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        public void ProfileNotFound_Handler(ErrorEventArgs args)
        {
            //    if (!StandardPopup.IsOpen) { ProfileError.Text = Alerts.PROFILE_SERVICE_FAILURE_MESSAGE; StandardPopup.IsOpen = true; }
        }

        private ScheduleItem currentSelectedItem;
        Panic objpanic;
        private string scheduleItemType;

        private void ContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            try
            {
                var selectedItem = (ScheduleItem)((ContextMenu)sender).DataContext;
                ContextMenuOpened((ContextMenu)sender, selectedItem);
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }
        private void ContenxtMenuEdit(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                ContextMenu openmenu = ContextMenuService.GetContextMenu((Button)sender);
                var selectedItem = (ScheduleItem)((FrameworkElement)sender).DataContext;
                ContextMenuOpened(openmenu, selectedItem);
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void ContextMenuOpened(ContextMenu openmenu, ScheduleItem selectedItem)
        {
            try
            {
                if (ViewModel.Settings == null)
                {
                    openmenu.IsOpen = false;
                    return;
                }
                if (ViewModel.HasInternetConnection)
                {
                    if (ViewModel.Settings.Schedule.Editable == false && ViewModel.Settings.Schedule.Cancellable == false)
                    {
                        MessageBox.Show("You don't have any cancel or edit permission");
                    }
                    else
                    {
                        currentSelectedItem = selectedItem;
                        scheduleItemType = currentSelectedItem.Type;


                        bool isCancelled = string.Equals(currentSelectedItem.State, "Cancelled", StringComparison.CurrentCultureIgnoreCase);
                        bool isNonScheduledTrip = string.IsNullOrEmpty(currentSelectedItem.State);

                        openmenu.Items.Clear();

                        if (ViewModel.Settings.Schedule.Editable)
                        {
                            MenuItem Edittrip = new MenuItem();
                            if (scheduleItemType.Equals("Login", StringComparison.InvariantCultureIgnoreCase))
                            {
                                Edittrip.Click += new System.Windows.RoutedEventHandler(this.OnEdit);
                                Edittrip.Header = "edit trip(login)";
                            }
                            else if (scheduleItemType.Equals("Logout", StringComparison.InvariantCultureIgnoreCase))
                            {
                                Edittrip.Click += new System.Windows.RoutedEventHandler(this.OnEdit);
                                Edittrip.Header = "edit trip(logout)";
                            }
                            openmenu.Items.Add(Edittrip);
                        }

                        if (ViewModel.Settings.Schedule.Cancellable && !isNonScheduledTrip && !isCancelled)
                        {
                            MenuItem Canceltrip = new MenuItem();
                            Canceltrip.Header = "cancel trip";
                            Canceltrip.Click += new System.Windows.RoutedEventHandler(this.OnCancel);
                            openmenu.Items.Add(Canceltrip);
                        }



                        if (openmenu.Items.Count == 0)
                        {
                            openmenu.IsOpen = false;
                            return;
                        }
                        else
                        {
                            if (openmenu.Items.Count == 1)
                                openmenu.Height = 100;
                            else
                                openmenu.Height = 200;
                            openmenu.IsOpen = true;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("You cannot edit trip without internet");
                }
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void OnEdit(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ViewModel.HasInternetConnection)
                {
                    //MessageBoxResult Edit = MessageBox.Show(AppResources.EDIT_SCHEDUELE_USER_COFIRM_MESSAGE, "Edit trip", MessageBoxButton.OKCancel);
                    AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                    objAnalyticEvent.EventName = FlurryConstants.Event.EditTrip_Click;
                    //call edittrip
                    LogAnalyticEvent(objAnalyticEvent);

                    if (currentSelectedItem != null)
                    {

                        if (ViewModel != null)
                        {
                            ViewModel.ContenxtMenuEdit(currentSelectedItem);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("You cannot edit trip without internet");
                }
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }
        private void OnCancel(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ViewModel.HasInternetConnection)
                {

                    CustomMessageBox messageBox = new CustomMessageBox()
                    {
                        Caption = "Cancel Trip",
                        Message = AppResources.CANCEL_SCHEDULE_USER_CONFIRM_MESSAGE,
                        LeftButtonContent = "Cancel",
                        RightButtonContent = "OK"
                    };


                    AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                    objAnalyticEvent.EventName = FlurryConstants.Event.CancelTrip_Click;
                    //call edittrip
                    LogAnalyticEvent(objAnalyticEvent);
                    if (currentSelectedItem != null)
                    {

                        if (ViewModel != null)
                        {

                            messageBox.Dismissed += (s1, e1) =>
                            {
                                switch (e1.Result)
                                {
                                    case CustomMessageBoxResult.LeftButton:
                                        // Do something.
                                        break;
                                    case CustomMessageBoxResult.RightButton:
                                        ViewModel.CancelSchedule(currentSelectedItem, scheduleItemType);
                                        break;
                                    default:
                                        break;
                                }
                            };

                            messageBox.Show();


                        }
                    }
                }
                else
                {
                    MessageBox.Show("You cannot cancel trip without internet");
                }
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }


        private void LocationSettings_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings-location:"));
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private async void Track(object sender, EventArgs e)
        {
            try
            {
                GoToTrackPanoramaItem();
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }

        }

        private void GoToTrackPanoramaItem()
        {
            try
            {
                DashboardPanorama.SetValue(Panorama.SelectedItemProperty, DashboardPanorama.Items[(1 + 1) % DashboardPanorama.Items.Count]);
                DashboardPanorama.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void Schedule(object sender, EventArgs e)
        {
            try
            {
                GotoSchedulePanormaItem();
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void GotoSchedulePanormaItem()
        {
            try
            {
                DashboardPanorama.SetValue(Panorama.SelectedItemProperty, DashboardPanorama.Items[(0 + 1) % DashboardPanorama.Items.Count]);
                DashboardPanorama.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }


        private void Profilepanaroma(object sender, EventArgs e)
        {
            try
            {
                GotoProfilePanorama();
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void GotoProfilePanorama()
        {
            try
            {
                DashboardPanorama.SetValue(Panorama.SelectedItemProperty, DashboardPanorama.Items[(2 + 1) % DashboardPanorama.Items.Count]);
                DashboardPanorama.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        bool hold = false;
        PCLTimer sosPCLTimer;

        private void ManipulationCompleted(object sender, System.Windows.Input.ManipulationCompletedEventArgs e)
        {
            try
            {
                
                SOSAlertPopCounter.Visibility = System.Windows.Visibility.Collapsed;

                if (sosPCLTimer != null)
                {
                    sosPCLTimer.Dispose();
                    sosPCLTimer = null;
                }
                SOSCounter = 5;

                AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                objAnalyticEvent.EventName = FlurryConstants.Event.SOSHold;
                objAnalyticEvent.Parameters.Add(new KeyValuePair<string, string>("PressTimeInSeconds", (DateTime.Now - holdSOSStartTime).TotalSeconds.ToString()));

                LogAnalyticEvent(objAnalyticEvent);
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }

        }

        private int SOSCounter { get; set; }
        DateTime holdSOSStartTime;
        private void ManipulationStarted(object sender, System.Windows.Input.ManipulationStartedEventArgs e)
        {
            try
            {
                if (ViewModel.RaisingSOS) { return; }
                SOSRaisedPanel.Visibility = Visibility.Collapsed;
                if (ViewModel.Settings != null && ViewModel.Settings.IsSOSEnabled && ViewModel.Settings.LongPressDuration.HasValue)
                {

                    SOSCounter = ViewModel.Settings.LongPressDuration.Value;
                    txtSOSCounter.Text = SOSCounter.ToString();
                    SOSAlertPopCounter.Visibility = System.Windows.Visibility.Visible;
                    holdSOSStartTime = DateTime.Now;

                    if (sosPCLTimer == null)
                    {
                        //set up timer to run every second
                        sosPCLTimer = new PCLTimer(new Action(sosTimer_Tick), TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
                    }
                    //timer starts one second from now
                    sosPCLTimer.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
                }
                else
                {
                    SOSAlertPopCounter.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void sosTimer_Tick()
        {
            try
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (SOSCounter > 0)
                    {
                        SOSCounter--;
                        txtSOSCounter.Text = SOSCounter.ToString();
                        if (SOSCounter == 0)
                        {
                            ViewModel.RaisePanicSOS(LocationEnabled);
                            SOSAlertPopCounter.Visibility = System.Windows.Visibility.Collapsed;

                            if (sosPCLTimer != null)
                            {
                                sosPCLTimer.Dispose();
                                sosPCLTimer = null;
                            }

                        }
                    }
                    else
                    {
                        txtSOSCounter.Text = 0.ToString();
                    }
                });
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }



        private void Privacypolicypage(object sender, EventArgs e)
        {
            try
            {
                ViewModel.NavigateToPrivacy();
                AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                objAnalyticEvent.EventName = FlurryConstants.Event.PrivacyPolicy_Click;
                LogAnalyticEvent(objAnalyticEvent);
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void Termsandconditions(object sender, EventArgs e)
        {
            try
            {
                ViewModel.NavigateToTermsAndConditions();
                AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                objAnalyticEvent.EventName = FlurryConstants.Event.TermsAndconditions_Click;
                LogAnalyticEvent(objAnalyticEvent);
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }



        private void Button_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                SOSRaisedPanel.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void DashboardPanorama_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (DashboardPanorama.SelectedItem == trackcab)
                {
                    trackcab.IsHitTestVisible = true;
                    AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                    objAnalyticEvent.EventName = FlurryConstants.Event.TrackCabPanorma_PageVisit;


                    //raise schedule page evnt
                    LogAnalyticEvent(objAnalyticEvent);
                }
                else
                {
                    trackcab.IsHitTestVisible = false;

                    if (DashboardPanorama.SelectedItem == ScheduleItem)
                    {
                        AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                        objAnalyticEvent.EventName = FlurryConstants.Event.SchedulePageVisit;
                        //raise schedule page evnt
                        LogAnalyticEvent(objAnalyticEvent);
                    }
                    if (DashboardPanorama.SelectedItem == Profile)
                    {
                        AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                        objAnalyticEvent.EventName = FlurryConstants.Event.ProfilePanorma_PageVisit;
                        //raise profile page visit evnt
                        LogAnalyticEvent(objAnalyticEvent);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }



        private void AbotUs_Click(object sender, EventArgs e)
        {
            try
            {
                ViewModel.NavigateToAboutUs();
                AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                objAnalyticEvent.EventName = FlurryConstants.Event.AboutUs_Click;
                LogAnalyticEvent(objAnalyticEvent);
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void CenterMapToCab_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (geoArr != null)
            {
                LocationRectangle setRect = null;
                setRect = LocationRectangle.CreateBoundingRectangle(geoArr);
                MapObj.SetView(setRect);
                mapCenterChanged = true;
            }
        }

        private void MapObj_Loaded(object sender, RoutedEventArgs e)
        {
            RegisterMap();
        }

        private void RegisterMap()
        {
            if (Constants.MapSettings.EnableMapRegistration)
            {
                Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = Constants.MapSettings.ApplicationId;
                Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = Constants.MapSettings.AuthenticationToken;
            }
        }

    }
}
