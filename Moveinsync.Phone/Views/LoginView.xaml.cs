﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Cirrious.MvvmCross.WindowsPhone.Views;
using Moveinsync.Core.ViewModels;
using Moveinsync.Core.EventArguments;
using System.Windows.Media;
using Microsoft.Phone.Net.NetworkInformation;
using Moveinsync.Core.Utility;
using System.Windows.Data;
using Moveinsync.Phone.Utility;
using Moveinsync.Phone.Resources;
using System.ComponentModel;
using Moveinsync.Core.Analytics;



namespace Moveinsync.Phone.Views
{
    public partial class LoginView : BasePhonePage
    {
        public LoginView()
        {
            if (DeviceNetworkInformation.IsNetworkAvailable)
            {
                string NetworkInfo = NetworkInterface.NetworkInterfaceType.ToString();
                var param_NetworkInfo = new List<FlurryWP8SDK.Models.Parameter>() { new FlurryWP8SDK.Models.Parameter("NetworkType", NetworkInfo) };
                FlurryWP8SDK.Api.LogEvent(Moveinsync.Core.Analytics.FlurryConstants.Event.NetworkType.ToString(), param_NetworkInfo);
            }
            else
            {
                var param_NoNetwork = new List<FlurryWP8SDK.Models.Parameter>() { new FlurryWP8SDK.Models.Parameter("NetworkType", "No_Network_Connectivity") };
                FlurryWP8SDK.Api.LogEvent(Moveinsync.Core.Analytics.FlurryConstants.Event.NetworkType.ToString(), param_NoNetwork);
                MessageBox.Show(AppResources.INTERNET_CONNECTIVITY_NOT_PRESENT_MESSAGE);

            }

            InitializeComponent();
        }

        public new LoginViewModel ViewModel
        {
            get { return (LoginViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            try
            {
                base.OnNavigatedTo(e);
                while (NavigationService.CanGoBack)
                {
                    NavigationService.RemoveBackEntry();
                }

                if (ViewModel.EvntRegistered == null)
                {
                    ViewModel.EvntRegistered += new EventHandler((object sender, EventArgs ea) =>
                    {
                        Registered((RegisterEventArgs)ea);
                    });
                }

                if (ViewModel.EvntValidatedOTP == null)
                {
                    ViewModel.EvntValidatedOTP += new EventHandler((object sender, EventArgs ea) =>
                    {
                        Validate((ValidateEventArgs)ea);
                    });
                }

                if (!string.IsNullOrWhiteSpace(ViewModel.SessionInvalidErrorMessage))
                {
                    SessionLoggedOut();
                }
                //if (ViewModel.EvntLogAnalytics != null)
                //{ 
                //    Moveinsync.Core.Analytics.AnalyticEvent objAnalyticEvent=new Moveinsync.Core.Analytics.AnalyticEvent();
                //    objAnalyticEvent.EventName = Moveinsync.Core.Analytics.FlurryConstants.Event.RegistrationPanorama_PageVisit;              
                //    LogAnalyticEvent(objAnalyticEvent);

                //}
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }

            if (ViewModel.EvntLogAnalytics != null)
            {
                Moveinsync.Core.Analytics.AnalyticEvent objAnalyticEvent = new Moveinsync.Core.Analytics.AnalyticEvent();
                objAnalyticEvent.EventName = Moveinsync.Core.Analytics.FlurryConstants.Event.Registration_PageVisit;
                LogAnalyticEvent(objAnalyticEvent);

            }


        }

        private void SessionLoggedOut()
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(ViewModel.SessionInvalidErrorMessage))
                {
                    MessageBox.Show("You have been logged out as device account is invalid. Please try to login again");
                    //MessageBox.Show(ViewModel.SessionInvalidErrorMessage);
                }
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void Validate(ValidateEventArgs e)
        {
            try
            {
                if (e.IsValidated == true)
                {
                    MessageBox.Show(AppResources.PASSWORD_VALIDATION_SERVICE_SUCCESS_MESSAGE);
                    Moveinsync.Core.Analytics.AnalyticEvent objAnalyticEvent = new Moveinsync.Core.Analytics.AnalyticEvent();
                    objAnalyticEvent.EventName = Moveinsync.Core.Analytics.FlurryConstants.Event.ConfirmOTP_Click;
                    LogAnalyticEvent(objAnalyticEvent);
                }
                else
                {
                    MessageBox.Show(e.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }


        private void Registered(RegisterEventArgs e)
        {
            try
            {
                if (e.IsRegisterd)
                {
                    //Code to redirect to OTP Page & disable the registration Page

                    MessageBox.Show(AppResources.OTP_REQUEST_SUCCESS_MESSAGE);


                    Moveinsync.Core.Analytics.AnalyticEvent objAnalyticEvent = new Moveinsync.Core.Analytics.AnalyticEvent();
                    objAnalyticEvent.EventName = Moveinsync.Core.Analytics.FlurryConstants.Event.OTPSuccessful;
                    LogAnalyticEvent(objAnalyticEvent);
                }
                else
                {
                    //Show error message in popup.
                    ShowError(e.ErrorMessage);
                    AnalyticEvent objAnalyticEvent = new AnalyticEvent();
                    objAnalyticEvent.EventName = Moveinsync.Core.Analytics.FlurryConstants.Event.OTPFailed;
                    //call for ValidatePanorama_PageVisit
                    LogAnalyticEvent(objAnalyticEvent);
                }
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }


        private void PhoneNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                ((TextBox)sender).UpdateBinding();
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void changeno_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txtOTP.Text = string.Empty;
                ViewModel.IsNewNumberRegistration = true;

                Moveinsync.Core.Analytics.AnalyticEvent objAnalyticEvent = new Moveinsync.Core.Analytics.AnalyticEvent();
                objAnalyticEvent.EventName = Moveinsync.Core.Analytics.FlurryConstants.Event.ChangeMobile_Click;
                LogAnalyticEvent(objAnalyticEvent);
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void txtOTP_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                ((TextBox)sender).UpdateBinding();
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        private void TextBlock_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                ViewModel.GoToTermsAndConditions();
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }

        //private void Movinsync_selectionChanged(object sender, SelectionChangedEventArgs e)
        //{

        //    if (moveinsync.SelectedItem == ENTEROTP)
        //    {

        //        ENTEROTP.IsHitTestVisible = true;
        //        AnalyticEvent objAnalyticEvent = new AnalyticEvent();
        //        objAnalyticEvent.EventName = FlurryConstants.Event.ValidatePanorama_PageVisit;

        //        LogAnalyticEvent(objAnalyticEvent);

        //    }

        //    if (moveinsync.SelectedItem == Registration)
        //    {
        //        AnalyticEvent objAnalyticEvent = new AnalyticEvent();

        //        objAnalyticEvent.EventName = FlurryConstants.Event.RegistrationPanorama_PageVisit;


        //        LogAnalyticEvent(objAnalyticEvent);
        //    }




        //}


    }
}