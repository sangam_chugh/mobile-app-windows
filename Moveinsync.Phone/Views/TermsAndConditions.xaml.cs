﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Moveinsync.Core.ViewModels;
using Moveinsync.Core.Utility;

namespace Moveinsync.Phone.Views
{
    public partial class TermsAndConditions : BasePhonePage
    {
        public TermsAndConditions()
        {
            InitializeComponent();
        }

        public new TermsAndConditionsViewModel ViewModel
        {
            get { return (TermsAndConditionsViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        private void MiniBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            try
            { 
            ProgressMeter.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                ViewModel.ThrowError(CommonErrorEventRaiser.RaiseErrorEvent(ex));
            }
        }
    }
}