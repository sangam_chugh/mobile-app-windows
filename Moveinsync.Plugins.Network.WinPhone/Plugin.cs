using System;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Plugins;


namespace Moveinsync.Plugins.Network.WindowsPhone{

    public class Plugin : IMvxPlugin {

        public void Load() {
            Mvx.RegisterSingleton<INetworkService>(new WinPhoneNetworkService());
        }
    }
}