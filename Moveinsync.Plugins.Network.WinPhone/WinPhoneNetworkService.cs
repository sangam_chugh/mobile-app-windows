﻿using System;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using Microsoft.Phone.Net.NetworkInformation;
using System.Threading;
using System.Net;


namespace Moveinsync.Plugins.Network.WindowsPhone{

    public class WinPhoneNetworkService : AbstractNetworkService {

        public WinPhoneNetworkService() {
            DeviceNetworkInformation.NetworkAvailabilityChanged += this.OnNetworkAvailabilityChanged;
            NetworkChange.NetworkAddressChanged += (sender, args) => {}; // this has to be listened to as well to hear previous event
            this.SetStatus(
                DeviceNetworkInformation.IsNetworkAvailable,
                DeviceNetworkInformation.IsWiFiEnabled,
                DeviceNetworkInformation.IsCellularDataEnabled,
                false
            );
        }


        private void OnNetworkAvailabilityChanged(object sender, NetworkNotificationEventArgs e) {
            this.SetStatus(
                DeviceNetworkInformation.IsNetworkAvailable,
                DeviceNetworkInformation.IsWiFiEnabled,
                DeviceNetworkInformation.IsCellularDataEnabled,
                true
            );
        }


        public async override Task<bool> IsHostReachable(string host, int msTimeout = 5000)
        {
            if (string.IsNullOrEmpty(host))
                throw new ArgumentNullException("host");

            return await Task.Run(() =>
            {
                var manualResetEvent = new ManualResetEvent(false);
                var reachable = false;
                DeviceNetworkInformation.ResolveHostNameAsync(new DnsEndPoint(host, 80), result =>
                {
                    reachable = result.NetworkInterface != null;
                    manualResetEvent.Set();
                }, null);
                manualResetEvent.WaitOne(TimeSpan.FromMilliseconds(msTimeout));
                return reachable;
            });
        }
    }
}
