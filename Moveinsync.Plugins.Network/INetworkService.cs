﻿using System;
using System.Threading.Tasks;
using Cirrious.MvvmCross.Plugins.Messenger;


namespace Moveinsync.Plugins.Network {

    public interface INetworkService {

        bool IsConnected { get; }
        bool IsWifi { get; }
        bool IsMobile { get; }
        Task<bool> IsHostReachable(string host, int msTimeout = 5000);
        MvxSubscriptionToken Subscribe(Action<NetworkStatusChangedMessage> action);
    }
}
